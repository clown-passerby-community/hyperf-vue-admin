<?php

declare(strict_types=1);

use Hyperf\HttpServer\Router\Router;

use App\Controller\Admin\Auth;
use App\Controller\Admin\AdminController;
use App\Controller\Admin\AdminRoleController;
use App\Controller\Admin\AdminMenuController;
use App\Controller\Admin\AdminLogController;
use App\Controller\Admin\AdminLoginLogController;
use App\Controller\Admin\BannerController;
use App\Controller\Admin\ConfigController;
use App\Controller\Admin\FriendLinkController;
use App\Controller\Admin\Index;
use App\Controller\Admin\UploadFileController;
use App\Controller\Admin\FileController;
use App\Controller\Admin\FileGroupController;
use App\Controller\Admin\VersionController;
use App\Controller\Admin\DatabaseController;

// 中间件
use App\Middleware\Admin\RabcMiddleware;
use App\Middleware\Admin\TokenAuthMiddleware;
use App\Middleware\CorsMiddleware;

Router::addGroup('/admin/', function(){
    Router::addGroup('auth/', function(){
        Router::post('login', [Auth::class, 'login']);
    });

    Router::addGroup('', function() {
        Router::addGroup('auth/', function(){
            Router::addRoute(['get', 'post'],'me', [Auth::class, 'me']);
            Router::addRoute(['get', 'post'],'getRabcList', [Auth::class, 'getRabcList']);
            Router::post('logout', [Auth::class, 'logout']);
        });
        // 首页统计
        Router::get('indexs', [Index::class, 'index']);
        // 按照日志类型的统计图数据
        Router::get('logsStatistics', [Index::class, 'logsStatistics']);
        // 版本的历史记录
        Router::get('versionLogs', [Index::class, 'versionLogs']);
        // 获取服务器状态
        Router::get('getServerStatus', [Index::class, 'getServerStatus']);

        // 月份表列表
        Router::get('get_month_lists', [Index::class, 'getMonthList']);

        // 文件上传
        Router::post('upload_file', [UploadFileController::class, 'file']);
        Router::post('upload_files', [UploadFileController::class, 'files']);

        // 获取文件列表
        Router::get('getFileList', [FileController::class, 'index']);
        // 删除指定文件
        Router::delete('files/delete', [FileController::class, 'delete']);
        // 移动文件到指定分组
        Router::put('files/removeFileGroup', [FileController::class, 'removeFileGroup']);


        // 文件分组管理
        Router::get('getGroupList', [FileGroupController::class, 'index']);
        Router::addGroup('fileGroup', function() {
            Router::post('/create', [FileGroupController::class, 'create']);
            Router::put('/update', [FileGroupController::class, 'update']);
            Router::delete('/delete', [FileGroupController::class, 'delete']);
        });

        Router::addGroup('', function() {
            // 文件管理
            Router::get('files', [FileController::class, 'index']);

            // 管理员
            Router::addGroup('admins', function(){
                Router::get('', [AdminController::class, 'index']);
                // 新增
                Router::post('/create', [AdminController::class, 'create']);
                // 编辑
                Router::put('/update', [AdminController::class, 'update']);
                // 删除
                Router::delete('/delete', [AdminController::class, 'delete']);
                // 指定字段变更
                Router::put('/changeFiledStatus', [AdminController::class, 'changeFiledStatus']);
                // 下拉列表
                Router::get('/getSelectLists', [AdminController::class, 'getSelectLists']);
            });

            // 角色
            Router::addGroup('admin_roles', function(){
                Router::get('', [AdminRoleController::class, 'index']);
                Router::post('/create', [AdminRoleController::class, 'create']);
                Router::put('/update', [AdminRoleController::class, 'update']);
                Router::delete('/delete', [AdminRoleController::class, 'delete']);
                Router::put('/changeFiledStatus', [AdminRoleController::class, 'changeFiledStatus']);
                Router::get('/getSelectLists', [AdminRoleController::class, 'getSelectLists']);
            });

            // 菜单管理
            Router::addGroup('admin_menus', function(){
                Router::get('', [AdminMenuController::class, 'index']);
                Router::post('/create', [AdminMenuController::class, 'create']);
                Router::put('/update', [AdminMenuController::class, 'update']);
                Router::delete('/delete', [AdminMenuController::class, 'delete']);
                Router::put('/changeFiledStatus', [AdminMenuController::class, 'changeFiledStatus']);
                Router::get('/getSelectLists', [AdminMenuController::class, 'getSelectLists']);
            });

            // 管理员日志
            Router::addGroup('admin_logs', function(){
                Router::get('', [AdminLogController::class, 'index']);
                Router::delete('/delete', [AdminLogController::class, 'delete']);
            });

            // 管理员登录日志
            Router::addGroup('admin_login_logs', function(){
                Router::get('', [AdminLoginLogController::class, 'index']);
                Router::delete('/delete', [AdminLoginLogController::class, 'delete']);
            });

            // 友情链接
            Router::addGroup('friendlinks', function(){
                Router::get('', [FriendLinkController::class, 'index']);
                Router::post('/create', [FriendLinkController::class, 'create']);
                Router::put('/update', [FriendLinkController::class, 'update']);
                Router::delete('/delete', [FriendLinkController::class, 'delete']);
                Router::put('/changeFiledStatus', [FriendLinkController::class, 'changeFiledStatus']);
            });

            // Banner
            Router::addGroup('banners', function(){
                Router::get('', [BannerController::class, 'index']);
                Router::post('/create', [BannerController::class, 'create']);
                Router::put('/update', [BannerController::class, 'update']);
                Router::delete('/delete', [BannerController::class, 'delete']);
                Router::put('/changeFiledStatus', [BannerController::class, 'changeFiledStatus']);
            });

            // 配置管理
            Router::addGroup('configs', function(){
                Router::get('', [ConfigController::class, 'index']);
                Router::post('/create', [ConfigController::class, 'create']);
                Router::put('/update', [ConfigController::class, 'update']);
                Router::delete('/delete', [ConfigController::class, 'delete']);
                Router::put('/changeFiledStatus', [ConfigController::class, 'changeFiledStatus']);
            });

            // 版本管理
            Router::addGroup('versions', function(){
                Router::get('', [VersionController::class, 'index']);
                Router::post('/create', [VersionController::class, 'create']);
                Router::put('/update', [VersionController::class, 'update']);
                Router::delete('/delete', [VersionController::class, 'delete']);
                Router::put('/changeFiledStatus', [VersionController::class, 'changeFiledStatus']);
            });

            // 数据库管理
            Router::addGroup('database', function() {
                // 数据表列表
                Router::get('/tables', [DatabaseController::class, 'index']);
                // 数据库备份
                Router::post('/backupsTables', [DatabaseController::class, 'backupsTables']);
                // 备份记录
                Router::get('/backups', [DatabaseController::class, 'backups']);
                // 删除指定备份记录
                Router::delete('/deleteBackup', [DatabaseController::class, 'deleteBackup']);
            });
        }, [
            // 权限验证
            'middleware' => [
                RabcMiddleware::class
            ],
        ]);
    }, [
        // 登录Token验证
        'middleware' => [
            TokenAuthMiddleware::class
        ],
    ]);
},
    [
        'middleware' => [
            CorsMiddleware::class,
        ],
    ]
);
