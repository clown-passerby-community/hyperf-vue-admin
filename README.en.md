# hyperf-vue-admin

### 作者
[小丑路人社区](https://bbs.cnpscy.com)

### Hyperf与Laravel版

<p align="center">
    <a href="https://gitee.com/clown-passerby-community/laravel-vue-admin" target="_blank">laravel-vue-admin</a>
</p>
<p align="center">
    <img src="https://gitee.com/clown-passerby-community/laravel-vue-admin/badge/star.svg?theme=dark" />
    <img src="https://gitee.com/clown-passerby-community/laravel-vue-admin/badge/fork.svg?theme=dark" />
    <img src="https://svg.hamm.cn/badge.svg?key=License&value=MIT&color=da4a00" />
</p>

<p align="center">
    <a href="https://gitee.com/clown-passerby-community/hyperf-vue-admin" target="_blank">Hyperf-后台管理</a>
</p>
<p align="center">
    <img src="https://gitee.com/clown-passerby-community/hyperf-vue-admin/badge/star.svg?theme=dark" />
    <img src="https://gitee.com/clown-passerby-community/hyperf-vue-admin/badge/fork.svg?theme=dark" />
    <img src="https://svg.hamm.cn/badge.svg?key=License&value=MIT&color=da4a00" />
</p>

### 环境需求
- Swoole >= 4.6.x 并关闭 `Short Name`
- PHP >= 8.0 并开启以下扩展：
    - mbstring
    - json
    - pdo
    - openssl
    - redis
    - pcntl
- Mysql >= 5.7
- Redis >= 4.0

### 介绍

![首页](public/demo/home.png)

![版本历史记录](public/demo/版本历史记录.png)

![西班牙语](public/demo/西班牙语.png)

![请求日志统计](public/demo/请求日志统计.png)

![图片选择器功能](public/demo/图片选择器.png)

![数据库管理](public/demo/数据库管理.png)

![备份管理](public/demo/备份管理.png)

### 使用说明

##### 启动服务，且热更新
* 命令行：`php bin/hyperf.php server:watch`
* www角色执行命令：`su -c "php bin/hyperf.php serve:watch" -s /bin/sh www`
* 后置进程：`nohup php 文件 2>&1 &`
* 使用`supervisor`守护进程启动服务即可

### 安装教程

##### PHP
* `composer install`
* 同步导入数据表：`php bin/hyperf.php sync:database:tables`

### Vue代码
[laravel-vue-element-admin](https://gitee.com/clown-passerby-community/laravel-vue-element-admin)
* 需删除vue项目`src->utils->auth.js`文件的`getToken`方法内的`'Bearer ' + `

### 捐助
如果您觉得我们的开源软件对你有所帮助，请扫下方二维码打赏我们一杯咖啡。

![Alipay payment code](https://images.gitee.com/uploads/images/2020/1112/091130_811b3a6c_790553.jpeg "alipay-400.jpg")
![Wechat collection code](https://images.gitee.com/uploads/images/2020/1112/091305_2592a352_790553.jpeg "wechat-400-width(1).jpg")
