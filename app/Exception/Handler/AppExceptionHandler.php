<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace App\Exception\Handler;

use App\Constants\StatusConst;
use App\Exception\Exception;
use App\Model\Log\Log;
use App\Traits\Json;
use Carbon\Exceptions\ParseErrorException;
use Hyperf\Contract\StdoutLoggerInterface;
use Hyperf\Event\Contract\ListenerInterface;
use Hyperf\ExceptionHandler\ExceptionHandler;
use Hyperf\HttpMessage\Exception\NotFoundHttpException;
use Hyperf\HttpMessage\Exception\ServerErrorHttpException;
use Hyperf\HttpMessage\Stream\SwooleStream;
use Hyperf\HttpServer\Router\Dispatched;
use Hyperf\Validation\ValidationException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Hyperf\HttpServer\Contract\RequestInterface;
use Throwable;
use Hyperf\HttpServer\Response;
use Hyperf\HttpServer\Exception\Handler\HttpExceptionHandler;

class AppExceptionHandler extends ExceptionHandler //implements ListenerInterface
{
    /**
     * @var \Psr\Http\Message\ServerRequestInterface
     */
    protected $request;
    /**
     * @var \Hyperf\HttpServer\Response
     */
    protected $response;

    /**
     * @var StdoutLoggerInterface
     */
    protected $logger;

    public function __construct(StdoutLoggerInterface $logger, ServerRequestInterface $request, Response $response)
    {
        $this->logger = $logger;
        $this->request = $request;
        $this->response = $response;
    }

    public function listenProcess($throwable, &$msg, &$http_status = false)
    {
        // 验证器的错误类
        if ( $throwable instanceof ValidationException ) {
            $msg = $throwable->validator->errors()->first();
        }else if ( $throwable instanceof Exception ) { // 自定义的异常类监听
            $msg = $throwable->getMessage();
        }else if ( $throwable instanceof \Exception ) { // 异常类监听
            $msg = $throwable->getMessage();
        }else if ( $throwable instanceof NotFoundHttpException){ // 404状态码监听
            $msg = 'HTTP URI 无效！';
        }else if ( $throwable instanceof ServerErrorHttpException){ // 500状态码监听
            $msg = '服务器内部错误！';
        }else if ( $throwable instanceof HttpExceptionHandler ) { // HTTP状态码异常类监听
            $http_status = (int)$throwable->getStatusCode();
            switch ($http_status){
                case 500:
                    $msg = '服务器内部错误！';
                    break;
                case 404:
                    $msg = 'HTTP URI 无效！';
                    break;
            }
        }

        // 获取控制器、方法、路由及中间件【前提：有效的路由】
        $dispatchedHandler = $this->request->getAttribute(Dispatched::class)->handler ?? [];

        // 设置HTTP的状态码
        $http_status = isset($http_status)
            ? $http_status
            : (
                method_exists($throwable, 'getCode')
                    ? $throwable->getCode()
                    : (
                        method_exists($throwable, 'getStatusCode')
                            ? $throwable->getStatusCode()
                            : 200
                    )
                );
        if ($http_status == 0) $http_status = 400;
        // 只要异常报错了，http_stauts就有问题！！！
        if ($http_status == '42S02' || $http_status == '42S22') $http_status = 400;

        // 记录日志
        if ($dispatchedHandler){
            $controller_and_method = $dispatchedHandler->callback;
            if (is_string($dispatchedHandler->callback)){
                $controller_and_method = explode('@', $dispatchedHandler->callback);
            }
        }


        Log::getInstance()->create([
            'api_url' => $this->request->url(),
            'method' => $this->request->getMethod(),
            'request_data' => $this->request->all(),
            'request_headers' => $this->request->getHeaders(),
            'error' => [
                'file' => $throwable->getFile(),
                'line' => $throwable->getLine(),
                'code' => $throwable->getCode(),
                'msg' => $throwable->getMessage(),
            ],
            'http_status' => $http_status,
            'created_ip' => get_real_ip($this->request),
            'browser_type' => $this->request->header('user-agent'),
            'controller_method' => $dispatchedHandler ? end($controller_and_method) : '',
            'route' => $dispatchedHandler ? $dispatchedHandler->route : '',
        ]);
    }

    public function handle(Throwable $throwable, ResponseInterface $response)
    {
        //阻止异常冒泡
        $this->stopPropagation();

        $msg = 'error';

        // 监听异常类
        $this->listenProcess($throwable, $msg, $http_status);

        $this->logger->error(sprintf('%s[%s] in %s', $msg, $throwable->getLine(), $throwable->getFile()));
        $this->logger->error($throwable->getTraceAsString());

        $data = [
            'msg' => $msg,
            'status' => StatusConst::ERROR,
            'data' => [],
            'error' => getenv('app_env') != 'prod' ? [
                'msg' => $throwable->getMessage(),
                'file' => $throwable->getFile(),
                'line' => $throwable->getLine(),
                'code' => $throwable->getCode(),
                'http_status' => $http_status,
            ] : []
        ];

        return $response->withStatus($http_status)->withBody(new SwooleStream(json_encode($data)));
    }

    public function isValid(Throwable $throwable): bool
    {
        return true;
    }
}
