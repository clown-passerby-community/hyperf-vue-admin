<?php

declare(strict_types=1);

namespace App\Command;

use Hyperf\Command\Command as HyperfCommand;
use Hyperf\Command\Annotation\Command;
use Hyperf\DbConnection\Db;
use Psr\Container\ContainerInterface;

/**
 * @Command
 */
#[Command]
class SyncDatabaseTables extends HyperfCommand
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        parent::__construct('sync:database:tables');
    }

    public function configure()
    {
        parent::configure();
        $this->setDescription('同步导入数据表');
    }

    public function handle()
    {
        $database = getenv('DB_DATABASE');
        if (!Db::select("SHOW DATABASES LIKE '{$database}'")){
            return $this->error('请先创建数据库`' . $database . '`');
        }

        $sync = true;
        $exist = DB::select("SHOW TABLES LIKE '" . getenv('DB_PREFIX') . "admins'");
        if ($exist){
            $sync = false;
            if ($this->confirm('已存在数据表，请选择是否强制覆盖数据库?')) {
                $sync = true;
            }else{
                $this->error('您已取消导入数据库');
            }
        }

        if ($sync){
            $this->info('数据导入中……');

            $result = DB::unprepared(file_get_contents(BASE_PATH . ('/../hyperf-vue-admin.sql')));

            if (!$result){
                $this->error('数据表导入失败！');
            }
            $this->info('数据表导入成功！');
        }

        // 执行：自动按月分表
        $this->autotablebuild();
    }

    private function autotablebuild()
    {
        // 执行自动分月，避免新安装用户确实按月分表
        $this->call('command:auto_table_build');
    }
}
