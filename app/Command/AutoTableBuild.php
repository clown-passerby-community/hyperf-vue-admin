<?php

declare(strict_types=1);

namespace App\Command;

use App\Logger;
use App\Model\Log\AdminLog;
use App\Model\Log\AdminLoginLog;
use App\Model\Log\Log;
use Hyperf\Command\Command as HyperfCommand;
use Psr\Container\ContainerInterface;
use Hyperf\Crontab\Annotation\Crontab;
use Hyperf\Command\Annotation\Command;

/**
 * 自动按月分表
 *
 * Class AutoTableBuild
 *
 *
 * 通过注解定义，触发定时任务
 * @Crontab(name="AutoTableBuild", rule="30 1 1 * *", callback="handle", memo="Automatically create Month by month table|自动创建按月分表")
 *
 * @package App\Command
 *
 * @Command
 */
class AutoTableBuild extends HyperfCommand
{
    protected $name = 'command:auto_table_build';

    /**
     * @var ContainerInterface
     */
    protected $container;

    private $logger;

    // 自动创建按月分表的定义
    private $tabel_list = [
        // Log::class,
        // AdminLog::class,
        // AdminLoginLog::class,
    ];

    public function configure()
    {
        parent::configure();
        $this->setDescription('按月分表自动生成（Automatic generation of monthly tables）');

        // 第一个参数对应日志的 name, 第二个参数对应 config/autoload/logger.php 内的 key
        $this->logger = Logger::get('按月分表自动生成', 'crontab');
    }

    public function handle()
    {
        $this->logger->info('auto_table_build - 自动按月分表 - start');

        foreach ($this->tabel_list as $table) {
            $model = new $table;
            $model->createMonthTable('', strtotime('+0 month'));
            $model->createMonthTable('', strtotime('+1 month'));
        }

        $this->logger->info('auto_table_build - 自动按月分表 - end');
    }
}
