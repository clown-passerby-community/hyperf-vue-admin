<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace App\Constants;

use Hyperf\Constants\AbstractConstants;
use Hyperf\Constants\Annotation\Constants;

/**
 * @Constants
 */
class ErrorCode extends AbstractConstants
{
    /**
     * @Message("Bad Request！")
     */
    public const BAD_REQUEST = 400;

    /**
     * @Message("认证失败！")
     */
    public const UNAUTHORIZED = 401;

    /**
     * @Message("Forbidden！")
     */
    public const FORBIDDEN = 403;

    /**
     * @Message("Server Error！")
     */
    public const SERVER_ERROR = 500;
}
