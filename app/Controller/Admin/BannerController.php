<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Model\System\Banner;
use App\Request\Admin\BannerRequest;
use Hyperf\Di\Annotation\Inject;

class BannerController extends BaseController
{
    /**
     * @Inject()
     * @var Banner
     */
    protected $model;

    /**
     * 验证器
     *
     * @Inject
     * @var BannerRequest
     */
    protected $validator;

    public function setFiltersWhere($build)
    {
        // 按照名称进行搜索
        if (!empty($search = $this->request->input('search', ''))){
            $build->where('banner_name', 'LIKE', '%' . trim($search) . '%');
        }
        // 状态
        $is_check = $this->request->input('is_check', -1);
        if ($is_check > -1){
            $build->where('is_check', '=', $is_check);
        }
    }
}
