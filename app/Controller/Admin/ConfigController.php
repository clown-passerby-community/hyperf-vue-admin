<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Model\System\Config;
use App\Request\Admin\ConfigRequest;
use Hyperf\Di\Annotation\Inject;
use Psr\Http\Message\ResponseInterface as PsrResponseInterface;

class ConfigController extends BaseController
{
    /**
     * @Inject()
     * @var Config
     */
    protected $model;

    /**
     * 验证器
     *
     * @Inject
     * @var ConfigRequest
     */
    protected $validator;

    public function setFiltersWhere($build)
    {
        // 按照名称进行搜索
        if (!empty($search = $this->request->input('search', ''))){
            $build->where('config_title', 'LIKE', '%' . trim($search) . '%')
                ->orWhere('config_name', 'LIKE', '%' . trim($search) . '%');
        }
        // 状态
        $is_check = $this->request->input('is_check', -1);
        if ($is_check > -1){
            $build->where('is_check', '=', $is_check);
        }
        // 类型
        $config_type = $this->request->input('config_type', -1);
        if ($config_type > -1){
            $build->where('config_type', '=', $config_type);
        }
        // 分组
        $config_group = $this->request->input('config_group', -1);
        if ($config_group > -1){
            $build->where('config_group', '=', $config_group);
        }
    }

    /**
     * 同步配置到配置文件
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function pushRefreshConfig(): PsrResponseInterface
    {
        $this->model->pushRefreshConfig();
        return $this->success([], '配置文件已同步成功！');
    }
}
