<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Model\Rabc\AdminRole;
use App\Request\Admin\AdminRoleRequest;
use Hyperf\Di\Annotation\Inject;

class AdminRoleController extends BaseController
{
    /**
     * @Inject()
     * @var AdminRole
     */
    protected $model;

    /**
     * 验证器
     *
     * @Inject
     * @var AdminRoleRequest
     */
    protected $validator;

    protected $with   = ['menus'];

    public function setFiltersWhere($build)
    {
        // 按照名称进行搜索
        if (!empty($search = $this->request->input('search', ''))){
            $build->where('role_name', 'LIKE', '%' . trim($search) . '%');
        }
        // 状态
        $is_check = $this->request->input('is_check', -1);
        if ($is_check > -1){
            $build->where('is_check', '=', $is_check);
        }
    }
}
