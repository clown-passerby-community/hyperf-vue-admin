<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Model\Log\AdminLoginLog;
use Hyperf\Di\Annotation\Inject;

class AdminLoginLogController extends BaseController
{
    /**
     * @Inject()
     * @var AdminLoginLog
     */
    protected $model;

    protected $with = ['admin'];

    public function setFiltersWhere($build)
    {
        $admin_id = $this->request->input('admin_id', 0);
        if ($admin_id) {
            $build->where('admin_id', $admin_id);
        }
        // 状态
        $is_check = $this->request->input('is_check', -1);
        if ($is_check > -1){
            $build->where('is_check', '=', $is_check);
        }
    }
}
