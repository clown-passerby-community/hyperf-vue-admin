<?php

declare(strict_types = 1);

namespace App\Controller\Admin;

use Hyperf\Di\Annotation\Inject;
use App\Exception\Exception;
use Psr\Http\Message\ResponseInterface;
use App\Model\UploadFile;

class UploadFileController extends BaseController
{
    /**
     * @Inject()
     * @var UploadFile
     */
    protected $uploadFile;

    /**
     * 单文件上传
     *
     * @param  string                        $file_name
     *
     * @return ResponseInterface
     */
    public function file(\League\Flysystem\Filesystem $filesystem, string $file_name = 'file'): ResponseInterface
    {
        // 存在则返回一个 Hyperf\HttpMessage\Upload\UploadedFile 对象，不存在则返回 null
        $file = $this->request->file($file_name);
        if (empty($file)) return $this->error('请上传文件！');

        try {
            $stream = fopen($file->getRealPath(), 'r+');

            // 获取文件扩展名
            $name = $this->uploadFile->getUniqidName($file->getExtension());
            $path_url = $this->uploadFile->getFilePath($name);

            $filesystem->writeStream(
                $path_url,
                $stream
            );
            fclose($stream);

            // 如果开启了第三方文件系统，那么URL域名要做相应的配置
            $upload_file = $this->uploadFile->addRecord($path_url, $file, getenv('FILE_SERVE'), getenv('FILESYSTEM_HOST'));

            return $this->success(['path_url' => $upload_file->file_url], '上传成功！');
        } catch (Exception $e) {
            return $this->error('上传失败！');
        }
    }

    /**
     * 批量文件上传
     *
     * @param  string                        $file_name
     *
     * @return ResponseInterface
     */
    public function files(\League\Flysystem\Filesystem $filesystem, string $file_name = 'files'): ResponseInterface
    {
        $files = $this->request->file($file_name);
        if (empty($files)) return $this->error('请上传文件！');

        foreach ($files as $file){
            $stream = fopen($file->getRealPath(), 'r+');

            $path_url = 'uploads/'. make_uuid() . '.' . $file->getExtension();

            $filesystem->writeStream(
                $path_url,
                $stream
            );
            fclose($stream);

            // 如果开启了第三方文件系统，那么URL域名要做相应的配置
            $upload_file = $this->uploadFile->addRecord($path_url, $file, getenv('FILE_SERVE'), getenv('FILESYSTEM_HOST'));

            $paths[] = $upload_file->file_url;
        }

        return $this->success($paths, '上传成功！');
    }
}
