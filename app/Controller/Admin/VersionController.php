<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Model\System\Version;
use App\Request\Admin\VersionRequest;
use Hyperf\Di\Annotation\Inject;

class VersionController extends BaseController
{
    /**
     * @Inject()
     * @var Version
     */
    protected $model;

    /**
     * 验证器
     *
     * @Inject
     * @var VersionRequest
     */
    protected $validator;

    public function setFiltersWhere($build)
    {
        // 按照名称进行搜索
        if (!empty($search = $this->request->input('search', ''))){
            $build->where(function($query) use ($search){
                $query->where('version_name', 'LIKE', '%' . trim($search) . '%')
                    ->orWhere('version_number', 'LIKE', '%' . trim($search) . '%');
            });
        }
    }
}
