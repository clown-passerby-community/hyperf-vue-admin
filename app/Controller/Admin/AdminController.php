<?php

declare(strict_types = 1);

namespace App\Controller\Admin;

use Hyperf\Di\Annotation\Inject;
use App\Request\Admin\AdminRequest;
use App\Model\Rabc\Admin;

class AdminController extends BaseController
{
    /**
     * @Inject()
     * @var Admin
     */
    protected $model;

    /**
     * 验证器
     *
     * @Inject
     * @var AdminRequest
     */
    protected $validator;

    protected $with = ['adminInfo'];

    public function setFiltersWhere($build)
    {
        // 按照名称进行搜索
        if (!empty($search = $this->request->input('search', ''))){
            $build->where('admin_name', 'LIKE', '%' . trim($search) . '%');
        }
        // 状态
        $is_check = $this->request->input('is_check', -1);
        if ($is_check > -1){
            $build->where('is_check', '=', $is_check);
        }
    }
}
