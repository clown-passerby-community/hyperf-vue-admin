<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Controller\AbstractController;
use App\Exception\Exception;
use Hyperf\Validation\Contract\ValidatorFactoryInterface;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\GetMapping;
use Psr\Http\Message\ResponseInterface as PsrResponseInterface;

class BaseController extends AbstractController
{
    use TraitController;

    protected $service;

    /**
     * 验证器
     */
    protected $validator;

    /**
     * @Inject()
     * @var ValidatorFactoryInterface
     */
    protected $validationFactory;

    public function getAdminId(): int
    {
        return $this->request->getAttribute('admin_id', 0);
    }

    public function index(): PsrResponseInterface
    {
        return $this->success($this->lists($this->request));
    }

    private function checkValidatorRule($scene)
    {
        $rules = $this->validator->getRules($scene);
        if ($rules){
            $validator = $this->validationFactory->make($this->request->all(), $rules, $this->validator->getMessages());
            if ( $validator->fails() ) {
                throw new Exception($validator->errors()->first());
            }
        }
    }

    public function create(): PsrResponseInterface
    {
        $params = $this->request->all();
        // 开启验证器
        if ( $this->validator ) {
            $this->checkValidatorRule(__FUNCTION__);
        }

        if ( $this->add($params) ) {
            return $this->success([], $this->getError());
        } else {
            throw new Exception($this->getError());
        }
    }

    public function update(): PsrResponseInterface
    {
        $params = $this->request->all();
        // 开启验证器
        if ( $this->validator ) {
            $this->checkValidatorRule(__FUNCTION__);
        }

        if ( $this->save($params) ) {
            return $this->success([], $this->getError());
        } else {
            throw new Exception($this->getError());
        }
    }

    /**
     * 删除与批量删除
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function delete(): PsrResponseInterface
    {
        $params = $this->request->all();
        // 开启验证器
        if ( $this->validator ) {
            $this->checkValidatorRule(__FUNCTION__);
        }

        if ( $this->batchDelete($params) ) {
            return $this->success([], $this->getError());
        } else {
            throw new Exception($this->getError());
        }
    }

    /**
     * 设置指定字段【常用于状态的变动】
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function changeFiledStatus(): PsrResponseInterface
    {
        $params = $this->request->all();
        // 开启验证器
        if ( $this->validator ) {
            $this->checkValidatorRule(__FUNCTION__);
        }

        if ( $this->patchFiled($params) ) {
            return $this->success([], $this->getError());
        } else {
            throw new Exception($this->getError());
        }
    }

    /**
     * 下拉筛选列表（可搜索）
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getSelectLists(): PsrResponseInterface
    {
        $lists = $this->selectLists();
        return $this->success($lists);
    }
}