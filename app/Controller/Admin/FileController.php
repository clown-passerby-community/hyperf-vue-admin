<?php

declare(strict_types = 1);

namespace App\Controller\Admin;

use App\Exception\Exception;
use App\Model\UploadFile;
use Hyperf\Di\Annotation\Inject;

class FileController extends BaseController
{
    /**
     * @Inject()
     * @var UploadFile
     */
    protected $model;

    public function setFiltersWhere($build)
    {
        // 按照名称进行搜索
        if (!empty($search = $this->request->input('search', ''))){
            $build->where('file_name', 'LIKE', '%' . trim($search) . '%');
        }
        // 状态
        $is_check = $this->request->input('is_check', -1);
        if ($is_check > -1){
            $build->where('is_check', '=', $is_check);
        }
        // 文件类型
        if (!empty($file_type = $this->request->input('file_type', ''))){
            $build->where('file_type', '=', $file_type);
        }
    }

    /**
     * 移动文件至指定分组
     */
    public function removeFileGroup()
    {
        if ($this->model->whereIn('file_id', $this->request->input('file_ids'))->update(['group_id' => $this->request->input('group_id')])){

            return $this->success([], $this->service->getError());
        }else{
            throw new Exception('移动失败！');
        }
    }
}
