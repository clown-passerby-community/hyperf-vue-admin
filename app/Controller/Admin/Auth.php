<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Controller\AbstractController;
use App\Request\Admin\LoginRequest;
use App\Services\Admin\AuthService;
use Hyperf\Di\Annotation\Inject;
use Psr\Http\Message\ResponseInterface as PsrResponseInterface;

class Auth extends BaseController
{
    /**
     * @Inject()
     * @var AuthService
     */
    protected $authService;

    /**
     * 登录
     *
     * @param  \App\Request\Admin\LoginRequest  $request
     *
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \App\Exception\Exception
     */
    public function login(LoginRequest $request): PsrResponseInterface
    {
        $result = $this->authService->login($request, $request->input('admin_name'), $request->input('password'));

        return $this->success($result, $this->authService->getError());
    }

    public function me(): PsrResponseInterface
    {
        return $this->success($this->authService->getLoginAdmin($this->getAdminId()));
    }

    public function getRabcList(): PsrResponseInterface
    {
        return $this->success($this->authService->getRabcList($this->getAdminId()));
    }

    public function logout(): PsrResponseInterface
    {
        return $this->success($this->authService->logout($this->request->getHeader('Authorization')));
    }
}
