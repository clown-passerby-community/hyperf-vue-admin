<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Services\Admin\DatabaseService;
use Hyperf\Di\Annotation\Inject;
use Psr\Http\Message\ResponseInterface as PsrResponseInterface;

class DatabaseController extends BaseController
{
    /**
     * @Inject()
     * @var DatabaseService
     */
    protected $service;

    public function lists($query = [], array $params = [])
    {
        return $this->service->lists($this->request->all());
    }

    /**
     * 批量备份指定的数据表
     */
    public function backupsTables(): PsrResponseInterface
    {
        set_time_limit(0);//防止超时
        $result = $this->service->backupsTables($this->request->input('tables_list'));
        return $this->success($result, $this->service->getError());
    }

    /**
     * 备份记录列表
     */
    public function backups(): PsrResponseInterface
    {
        return $this->success($this->service->getBackupRecords());
    }

    /**
     * 删除指定的备份记录，并删除对应的备份文件
     */
    public function deleteBackup(): PsrResponseInterface
    {
        $this->service->deleteBackup((int)$this->request->input('backup_id'));

        return $this->success([], $this->service->getError());
    }
}
