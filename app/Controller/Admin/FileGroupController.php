<?php

declare(strict_types = 1);

namespace App\Controller\Admin;

use App\Model\UploadGroup;
use Hyperf\Di\Annotation\Inject;

class FileGroupController extends BaseController
{
    /**
     * @Inject()
     * @var UploadGroup
     */
    protected $model;

    public function setFiltersWhere($build)
    {
        // 按照名称进行搜索
        if (!empty($search = $this->request->input('search', ''))){
            $build->where('group_name', 'LIKE', '%' . trim($search) . '%');
        }
    }
}
