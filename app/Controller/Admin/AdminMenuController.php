<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Model\Rabc\AdminMenu;
use App\Request\Admin\AdminMenuRequest;
use Hyperf\Di\Annotation\Inject;

class AdminMenuController extends BaseController
{
    /**
     * @Inject()
     * @var AdminMenu
     */
    protected $model;

    /**
     * 验证器
     *
     * @Inject
     * @var AdminMenuRequest
     */
    protected $validator;

    public function lists($query = [], array $params = [])
    {
        $list = $this->model->getSelectLists();
        foreach ($list as &$v){
            $v['has_child'] = empty($v['_child']) ? false : true;
        }
        return $list;
    }
}
