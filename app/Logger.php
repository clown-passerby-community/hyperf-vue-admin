<?php

namespace App;

// use Hyperf\Logger\Logger;
use Hyperf\Logger\LoggerFactory;
use Hyperf\Utils\ApplicationContext;

/**
 * Class LoggerFactory
 *
 * 封装 Log 类
 *
 * @package App
 */
class Logger
{
    // 默认使用 Channel 名为 app 来记录日志，您也可以通过使用 Log::get($name) 方法获得不同 Channel 的 Logger
    public static function get(string $name = 'hyperf', $group = 'default')
    {
        return ApplicationContext::getContainer()->get(LoggerFactory::class)->get($name, $group);
    }
}
