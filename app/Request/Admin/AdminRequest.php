<?php

declare(strict_types = 1);

namespace App\Request\Admin;

use App\Model\Rabc\Admin;
use App\Request\BaseRequest;
use Hyperf\Validation\Rule;

class AdminRequest extends BaseRequest
{
    public function setModelInstance()
    {
        $this->model = Admin::getInstance();
    }

    public function rules() : array
    {
        return [
            'admin_id'   => 'required',
            'admin_name' => [
                'required',
                'max:200',
                'unique:' . $this->model->getTable() . ',admin_name' . $this->validate_id
            ],
            // 'admin_email' => 'required|max:200|email',
            'admin_head' => 'required',
            'is_check'   => 'required|in:0,1',
        ];
    }

    public function messages() : array
    {
        return [
            'admin_id.required'    => '管理员Id为必填项！',
            'admin_name.required'  => '管理员账户为必填项！',
            'admin_name.unique'    => '管理员账户已存在！',
            'admin_email.required' => '管理员邮箱为必填项！',
            'admin_email.email'    => '管理员邮箱格式非法！',
            'admin_head.required'  => '请设置管理员头像！',
            'is_check.required'    => '请设置是否启用！',
            'is_check.in'          => '请设置有效启用状态！',
        ];
    }

    protected $scene = [
        'create' => [
            'admin_name',
            'admin_email',
            'admin_head',
            'is_check',
        ],
        'update' => [
            'admin_id',
            'admin_name',
            'admin_email',
            'admin_head',
            'is_check',
        ],
    ];
}
