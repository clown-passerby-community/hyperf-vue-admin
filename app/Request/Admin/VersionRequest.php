<?php

declare(strict_types = 1);

namespace App\Request\Admin;

use App\Model\System\Version;
use App\Request\BaseRequest;

class VersionRequest extends BaseRequest
{
    public function setModelInstance()
    {
        $this->model = Version::getInstance();
    }

    public function rules() : array
    {
        return [
            'version_name'    => [
                'required',
                'max:256',
                'unique:' . $this->instance->getTable() . ',version_name' . $this->validate_id,
            ],
            'version_number'  => [
                'required',
                'unique:' . $this->instance->getTable() . ',version_number' . $this->validate_id,
            ],
            'publish_date' => [
                'date_format:Y-m-d H:i:s'
            ],
            'version_content' => [
                'required',
            ],
            'version_sort'    => [
                'min:0',
            ],
        ];
    }

    public function messages() : array
    {
        return [
            'version_name.required'    => '请输入版本名称！',
            'version_name.unique'      => '版本名称已存在，请更换！',
            'version_number.required'  => '请输入版本号！',
            'version_number.unique'    => '版本号已存在，请更换！',
            'publish_date.date_format'  => '请选择有效的发布时间！',
            'version_content.required' => '请输入版本内容！',
        ];
    }

    protected $scene = [
        'create' => [
            'version_name',
            'version_number',
            'publish_date',
            'version_content',
        ],
        'update' => [
            'version_id',
            'version_name',
            'version_number',
            'publish_date',
            'version_content',
        ],
    ];
}
