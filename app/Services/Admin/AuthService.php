<?php

declare(strict_types = 1);

namespace App\Services\Admin;

use App\Constants\CacheKey;
use App\Event\Admin\AdminLoginEvent;
use App\Exception\Exception;
use App\Library\Encrypt\Rsa;
use App\Model\Rabc\Admin;
use App\Model\Rabc\AdminMenu;
use App\Services\Service;
use Hyperf\Di\Annotation\Inject;
use Psr\EventDispatcher\EventDispatcherInterface;

class AuthService extends Service
{
    /**
     * @Inject()
     * @var Admin
     */
    protected $adminModel;

    /**
     * @Inject
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * 用户登录
     *
     * @param          $request
     * @param  string  $username
     * @param  string  $password
     *
     * @return array
     * @throws \App\Exception\Exception
     */
    public function login($request, string $username, string $password)
    {
        $admin = $this->adminModel->getUserByName($username);
        if ( !$admin ) throw new Exception('管理员账户不存在！');
        if ( !hash_verify($password, $admin->password) ) throw new Exception('管理员信息不匹配！');
        switch ($admin->is_check) {
            case 0:
                throw new Exception('该管理员尚未启用！');
                break;
            case 2:
                throw new Exception('该管理员已禁用！');
                break;
        }

        // 事件需要通过 事件调度器(EventDispatcher) 调度才能让 监听器(Listener) 监听到
        // 这里 dispatch(object $event) 会逐个运行监听该事件的监听器
        $this->eventDispatcher->dispatch(new AdminLoginEvent($request, $admin));

        $data = $this->getTokenFormat($admin);
        $token = Rsa::publicEncrypt($data);

        $this->setError('登录成功！');
        return [
            'access_token' => $token,
            'expire_time' => $data['expire_time'],
        ];
    }

    public function getTokenFormat($admin) : array
    {
        return ['admin_id' => $admin->admin_id, 'guard' => 'admin', 'expire_time' => time() + CacheKey::KEY_DEFAULT_TIMEOUT];
    }

    public function getLoginAdmin(int $admin_id)
    {
        $admin = $this->adminModel->find($admin_id);
        // vue-element-admin限制问题
        $admin['roles'] = ['admin'];
        return $admin;
    }

    public function getRabcList(int $admin_id)
    {
        // 如果是admin_id = 1，那么默认返回全部权限
        if($admin_id == 1){
            return list_to_tree(AdminMenu::getInstance()->getAllMenus()->toArray());
        }
        $admin = Admin::with(['roles.menus'])->find($admin_id)->toArray();

        $menus = [];
        foreach (array_column($admin['roles'], 'menus') as $item){
            $menus = array_merge($menus, $item);
        }

        return list_to_tree($menus);
    }
}
