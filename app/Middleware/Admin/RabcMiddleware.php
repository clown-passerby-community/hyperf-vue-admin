<?php

declare(strict_types = 1);

namespace App\Middleware\Admin;

use App\Constants\ErrorCode;
use App\Constants\StatusConst;
use App\Exception\Exception;
use App\Model\Rabc\Admin;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use App\Model\Rabc\AdminMenu;
use App\Model\Rabc\RoleWithMenu;

use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\Di\Annotation\Inject;

class RabcMiddleware implements MiddlewareInterface
{
    /**
     * @Inject
     * @var RequestInterface
     */
    protected $requestInterface;

    /**
     * @var ContainerInterface
     */
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler) : ResponseInterface
    {
        // return $handler->handle($request);


        // 请求方式
        $method = $this->requestInterface->getMethod();
        // 获取当前路由
        $route_path = trim($this->requestInterface->getPathInfo(), '/');
        // 开始验证路由权限
        if (!$this->checkRabc($request->getAttribute('admin_id'), $route_path)){
            throw new Exception('无权限', ErrorCode::FORBIDDEN);
        }
        return $handler->handle($request);
    }

    private function checkRabc(int $admin_id, string $route_path):bool
    {
        if ($admin_id == 1) return true;

        $role_ids = Admin::find($admin_id)->roles()->get()->pluck('role_id');
        if (empty($role_ids)) return false;

        $menu_ids = RoleWithMenu::getInstance()->getMenuIdsByRoles($role_ids);
        $menus = AdminMenu::getMenusByIds($menu_ids);
        $menus = array_column($menus, 'api_url', 'api_url');
        // foreach ($roles as $role){
        //     $menus[] = array_column($role->menus, 'api_url', 'api_url');
        // }
        if (empty($menus)) return false;
        if (array_key_exists($route_path, $menus)) return true;

        return false;
    }
}