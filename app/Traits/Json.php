<?php

declare(strict_types = 1);

namespace App\Traits;

use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Contract\ResponseInterface;

trait Json
{
    /**
     * @Inject
     * @var ResponseInterface
     */
    protected $responseInterface;

    protected $data = [];

    protected $msg = 'success';

    protected $status = 1;

    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    public function setMsg(string $msg): void
    {
        $this->msg = $msg;
    }

    public function setData(array $data): void
    {
        $this->data = $data;
    }

    public function success($data = [], $msg = 'success', $other = [])
    {
        $this->data = $data;
        $this->msg = $msg;
        $this->status = 1;
        return $this->myAjaxReturn($other);
    }

    public function error($msg = 'error', $status = 0, $data = [], $other = [])
    {
        $this->data = $data;
        $this->msg = $msg;
        $this->status = $status;
        return $this->myAjaxReturn($other);
    }

    public function setResponseServer($response)
    {
        if ($response){
            $this->responseInterface = $response;
        }
    }

    public function myAjaxReturn($other)
    {
        $data = array_merge([
            'data' => $this->data,
            'status' => $this->status,
            'msg' => $this->msg
        ], $other);

        return $this->responseInterface->json($data);
    }
}
