<?php

use Hyperf\Contract\StdoutLoggerInterface;
use Hyperf\Logger\LoggerFactory;
use Hyperf\ExceptionHandler\Formatter\FormatterInterface;
use Hyperf\Redis\RedisFactory;
use Hyperf\Utils\ApplicationContext;
use Hyperf\HttpServer\Contract\RequestInterface;

// 获取url地址
    function route($url = '', array $params = []): string
    {
        $query = http_build_query($params);
        if ($query) $query = '?' . $query;
        return trim(getenv('API_URL'), '/') . '/' . trim($url, '/') . $query;
    }

    // 获取url地址
    function web_route($url, array $params = []): string
    {
        $query = http_build_query($params);
        if ($query) $query = '?' . $query;
        return trim(getenv('APP_URL'), '/') . '/' . $url . $query;
    }
    
/**
 * 获取Container
 */
if ( !function_exists('di') ) {
    /**
     * Finds an entry of the container by its identifier and returns it.
     *
     * @param  null|mixed  $id
     *
     * @return mixed|\Psr\Container\ContainerInterface
     */
    function di($id = null)
    {
        $container = ApplicationContext::getContainer();
        if ( $id ) {
            return $container->get($id);
        }
        return $container;
    }
}

if ( !function_exists('container') ) {
    /**
     * 容器示例
     *
     * @return \Psr\Container\ContainerInterface
     */
    function container()
    {
        return ApplicationContext::getContainer();
    }
}

if ( !function_exists('format_throwable') ) {
    /**
     * Format a throwable to string.
     *
     * @param  Throwable  $throwable
     *
     * @return string
     */
    function format_throwable(Throwable $throwable) : string
    {
        return di()->get(FormatterInterface::class)->format($throwable);
    }
}

if ( !function_exists('redis') ) {
    function redis($name = 'default')
    {
        return di()->get(RedisFactory::class)->get($name);
    }
}

/**
 * 获取图片的真实地址
 */
function get_image_url($image){
    return env('API_URL') . '/storage/' . $image;
}

function get_real_ip($request = ''): string
{
    if (empty($request)){
        $request = di(RequestInterface::class);
    }
    $headers = $request->getHeaders();

    if(isset($headers['x-forwarded-for'][0]) && !empty($headers['x-forwarded-for'][0])) {
        return $headers['x-forwarded-for'][0];
    } elseif (isset($headers['x-real-ip'][0]) && !empty($headers['x-real-ip'][0])) {
        return $headers['x-real-ip'][0];
    }

    $serverParams = $request->getServerParams();
    if(isset($serverParams['http_client_ip'])){
        return $serverParams['http_client_ip'];
    }elseif(isset($serverParams['http_x_real_ip'])){
        return $serverParams['http_x_real_ip'];
    }elseif(isset($serverParams['http_x_forwarded_for'])){
        //部分CDN会获取多层代理IP，所以转成数组取第一个值
        $arr = explode(',',$serverParams['http_x_forwarded_for']);
        return $arr[0];
    }else{
        return $serverParams['remote_addr'];
    }
}