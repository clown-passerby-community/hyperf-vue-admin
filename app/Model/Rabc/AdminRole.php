<?php

declare (strict_types = 1);

namespace App\Model\Rabc;

use App\Model\Model;

class AdminRole extends Model
{
    protected $primaryKey = 'role_id';

    public function menus()
    {
        return $this->belongsToMany(AdminMenu::class, 'role_with_menus', 'role_id', 'menu_id')
                    ->where(['is_delete' => 0, 'is_check' => 1])
                    ->orderBy('menu_sort', 'ASC');
    }
}