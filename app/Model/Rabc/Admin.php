<?php

declare (strict_types = 1);

namespace App\Model\Rabc;

use App\Model\Model;
use Hyperf\Database\Model\Events\Creating;

/**
 */
class Admin extends Model
{
    protected $primaryKey = 'admin_id';
    public    $timestamps = false;

    protected $hidden = [
        'password'
    ];

    public function adminInfo()
    {
        return $this->hasOne(AdminInfo::class, $this->primaryKey, $this->primaryKey);
    }

    public function roles()
    {
        return $this->belongsToMany(AdminRole::class, 'admin_with_roles', $this->primaryKey, 'role_id');
    }

    public function setPasswordAttribute($value)
    {
        if ( !empty($value) ) $value = 123456;
        return $this->attributes['password'] = hash_encryption($value);
    }

    public function creating(Creating $event)
    {
        if ( empty($this->attributes['password']) ) $this->attributes['password'] = 123456;
    }

    /**
     * 通过管理员账号进行搜索
     *
     * @param  string  $admin_name
     *
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public function getUserByName(string $admin_name)
    {
        return $this->query()->where('admin_name', $admin_name)->first();
    }
}
