<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace App\Model;

use App\Model\SoftDelete\SoftDelete;
use App\Traits\Instance;
use App\Traits\ModelTrait;
use App\Traits\MysqlTable;
use Hyperf\DbConnection\Model\Model as BaseModel;
use Hyperf\Database\Model\Builder;

abstract class Model extends BaseModel
{
    use SoftDelete;
    use Instance;
    use ModelTrait;
    use MysqlTable;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
    // 时间字段的类型：时间戳
    protected $dateFormat = 'U';
    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'created_time';
    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'updated_time';
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'default';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    // 黑名单
    protected $guarded = [];


    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends      = [];
    // 开启月分表
    const MONTH_SUB_TABLE = false;
    // 定义按月分表的组成部分，避免逻辑报错
    const MIN_TABLE    = '';
    const MONTH_FORMAT = '';
    public function setMonthTable(string $month = '')
    {
        return $this;
    }
}
