<?php

namespace App\Model;

class UploadFile extends Model
{
    protected $primaryKey = 'file_id';
    protected $is_delete = 0;

    protected $appends = ['file_url'];

    public function getFileUrlAttribute($key)
    {
        if ($this->attributes['storage'] == 'local'){
            return route($this->attributes['file_name']);
        }
        // 非本地文件，获取文件时自动追加域名
        return $this->attributes['host_url'] . '/' . trim($this->attributes['file_name'],'/');
    }

    public function setFileNameAttribute($key): void
    {
        // 非本地文件上传，存储文件时自动移除域名
        if ($this->attributes['storage'] != 'local'){
            $key = str_replace($this->attributes['host_url'], '', $key);
        }
        $this->attributes['file_name'] = $key;
    }

    /**
     * 添加文件库上传记录
     *
     * @param  string  $file_name
     * @param          $file
     * @param  string  $storage  存储引擎
     * @param  string  $host_url 存储域名
     *
     * @return UploadFile
     */
    public static function addRecord(string $file_name, $file, $storage = 'local', $host_url = '', int $file_size = 0, $file_type = 'image/jepg', $extension = 'jpg')
    {
        // 添加文件库记录
        return UploadFile::create([
            'storage' => $storage,
            'host_url' => $storage == 'local' ? '' : $host_url,
            'file_name' => $file_name,
            'file_size' => is_string($file) ? $file_size : $file->getSize(),
            'file_type' => is_string($file) ? $file_type : $file->getMimeType(),
            'extension' => is_string($file) ? $extension : $file->getExtension(),
        ]);
    }

    /**
     * 图片的访问路径
     *
     * @param  string  $name
     *
     * @return string
     */
    public static function getFilePath(string $name) : string
    {
        return 'uploads/' . date('Y-m-d') . '/' . $name;
    }

    /**
     * 图片在服务端的绝对路径
     *
     * @param  string  $name
     *
     * @return string
     */
    public static function getAbsolutePath(string $name = '') : string
    {
        $dir_path = BASE_PATH . '/public/uploads/' . date('Y-m-d');
        $dir_prefix = '';
        foreach (explode('/', $dir_path) as $dir) {
            $dir_prefix .= '/' . $dir;
            if ( !is_dir($dir_prefix) ) mkdir($dir_prefix, 0755);
        }
        return $dir_path . '/' . (empty($name) ? '' : $name);
    }

    /**
     * 生成唯一的名称
     *
     * @param  string  $extension
     * @param  string  $type
     * @param  int     $length
     *
     * @return string
     */
    public static function getUniqidName(string $extension = 'png', string $type = 'img', int $length = 20) : string
    {
        return rand_str(20) . '.' . $extension;
        return uniqid($type, true) . date('Ymdhis') . '.' . $extension;
    }

    /**
     * 按照Ids进行批量搜索
     *
     * @param $file_ids
     *
     * @return array
     */
    public static function getFilePathByIds($file_ids)
    {
        $list = self::getInstance()
            ->cnpscyWhereIn('file_id', $file_ids)
            ->select('file_id', 'file_name', 'storage', 'file_url')
            ->get()
            ->toArray();
        return array_column($list, 'file_path', 'file_id');
    }
}
