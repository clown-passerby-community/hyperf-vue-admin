<?php

declare (strict_types = 1);

namespace App\Model\Log;

use App\Model\Model;
use App\Model\MonthModel;
use App\Model\Rabc\Admin;

/**
 */
class Log extends Model
{
    protected $primaryKey = 'log_id';
    public    $is_delete  = 1;// 是否删除：0.假删除；1.真删除【默认全部假删除】

    protected $casts = [
        'request_data' => 'array',
        'request_headers' => 'array',
        'error' => 'array',
    ];

    public function admin()
    {
        return $this->belongsTo(Admin::class, 'admin_id', 'admin_id');
    }
}
