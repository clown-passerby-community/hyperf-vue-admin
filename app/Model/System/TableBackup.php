<?php

declare (strict_types = 1);

namespace App\Model\System;

use App\Model\Model;

class TableBackup extends Model
{
    protected $primaryKey = 'backup_id';
    public    $is_delete    = 0;// 是否删除：0.假删除；1.真删除【默认全部假删除】
}
