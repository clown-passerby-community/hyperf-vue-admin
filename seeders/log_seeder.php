<?php

declare(strict_types=1);

use Hyperf\Database\Seeders\Seeder;

class LogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $db = \Hyperf\DbConnection\Db::table('logs');
        $start_time = microtime(true);
        for ($i = 0; $i < 10000; $i++)
        {
            $data = [];
            for ($j = 0; $j < 100; $j++)
            {
                $data[] = [
                    'user_id' => rand(1, 10000000),
                    'log_type' => rand(0, 2),
                    'login_type' => 0,
                    'log_status' => rand(0, 1),
                    'log_description' => rand_str(15),
                    'created_time' => time(),
                    'updated_time' => time(),
                ];
            }
            // 批量插入
            $db->insert($data);
        }
        // var_dump('用时：');
        var_dump(microtime(true) - $start_time);
    }
}
