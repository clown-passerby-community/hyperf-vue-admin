/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50738
 Source Host           : localhost:3306
 Source Schema         : hyperf-vue-admin

 Target Server Type    : MySQL
 Target Server Version : 50738
 File Encoding         : 65001

 Date: 08/07/2022 19:19:11
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for cnpscy_admin_infos
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_admin_infos`;
CREATE TABLE `cnpscy_admin_infos` (
  `admin_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '管理员信息表',
  `login_num` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '登录次数',
  `created_ip` varchar(20) NOT NULL DEFAULT '' COMMENT '创建时的IP',
  `browser_type` varchar(200) NOT NULL DEFAULT '' COMMENT '创建时浏览器类型',
  `created_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  KEY `admin_infos_admin_id_index` (`admin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cnpscy_admin_infos
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_admin_login_logs
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_admin_login_logs`;
CREATE TABLE `cnpscy_admin_login_logs` (
  `log_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '管理员登录日志',
  `admin_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Id',
  `log_status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '登录状态：1：成功；0：失败',
  `log_description` varchar(200) NOT NULL DEFAULT '' COMMENT '描述',
  `request_url` varchar(200) NOT NULL DEFAULT '' COMMENT '请求方法/路由',
  `log_method` varchar(20) NOT NULL DEFAULT '' COMMENT '请求类型/请求方式',
  `request_data` varchar(500) NOT NULL DEFAULT '' COMMENT '请求参数',
  `created_ip` varchar(20) NOT NULL DEFAULT '' COMMENT '创建时的IP',
  `browser_type` varchar(200) NOT NULL DEFAULT '' COMMENT '创建时浏览器类型',
  `is_delete` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除：1：删除；0：正常',
  `created_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`log_id`),
  KEY `admin_login_logs_admin_id_index` (`admin_id`),
  KEY `admin_login_logs_log_status_index` (`log_status`),
  KEY `admin_login_logs_is_delete_index` (`is_delete`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cnpscy_admin_login_logs
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_admin_login_logs_2022_06
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_admin_login_logs_2022_06`;
CREATE TABLE `cnpscy_admin_login_logs_2022_06` (
  `log_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '管理员登录日志',
  `admin_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Id',
  `log_status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '登录状态：1：成功；0：失败',
  `log_description` varchar(200) NOT NULL DEFAULT '' COMMENT '描述',
  `request_url` varchar(200) NOT NULL DEFAULT '' COMMENT '请求方法/路由',
  `log_method` varchar(20) NOT NULL DEFAULT '' COMMENT '请求类型/请求方式',
  `request_data` varchar(500) NOT NULL DEFAULT '' COMMENT '请求参数',
  `created_ip` varchar(20) NOT NULL DEFAULT '' COMMENT '创建时的IP',
  `browser_type` varchar(200) NOT NULL DEFAULT '' COMMENT '创建时浏览器类型',
  `is_delete` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除：1：删除；0：正常',
  `created_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`log_id`),
  KEY `admin_login_logs_admin_id_index` (`admin_id`),
  KEY `admin_login_logs_log_status_index` (`log_status`),
  KEY `admin_login_logs_is_delete_index` (`is_delete`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cnpscy_admin_login_logs_2022_06
-- ----------------------------
BEGIN;
INSERT INTO `cnpscy_admin_login_logs_2022_06` (`log_id`, `admin_id`, `log_status`, `log_description`, `request_url`, `log_method`, `request_data`, `created_ip`, `browser_type`, `is_delete`, `created_time`, `updated_time`) VALUES (1, 1, 1, '登录成功', '/admin/auth/login', 'POST', '{\"admin_name\":\"admin\",\"password\":\"123456\",\"app_key\":\"34D2DEDC049E4D726D83E4E2B1F34D9171\",\"app_type\":\"0\",\"timestamp\":\"1617288677\",\"nonce\":\"123456789\",\"sign\":\"2A71D4F6CD5F52A602A5981E41876D83\"}', '0.0.0.0', '', 0, 1654829941, 1654829941);
INSERT INTO `cnpscy_admin_login_logs_2022_06` (`log_id`, `admin_id`, `log_status`, `log_description`, `request_url`, `log_method`, `request_data`, `created_ip`, `browser_type`, `is_delete`, `created_time`, `updated_time`) VALUES (2, 1, 1, '登录成功', '/admin/auth/login', 'POST', '{\"admin_name\":\"admin\",\"password\":\"123456\",\"app_key\":\"34D2DEDC049E4D726D83E4E2B1F34D9171\",\"app_type\":\"0\",\"timestamp\":\"1617288677\",\"nonce\":\"123456789\",\"sign\":\"2A71D4F6CD5F52A602A5981E41876D83\"}', '0.0.0.0', '', 0, 1654829953, 1654829953);
INSERT INTO `cnpscy_admin_login_logs_2022_06` (`log_id`, `admin_id`, `log_status`, `log_description`, `request_url`, `log_method`, `request_data`, `created_ip`, `browser_type`, `is_delete`, `created_time`, `updated_time`) VALUES (3, 1, 1, '登录成功', '/admin/auth/login', 'POST', '{\"admin_name\":\"admin\",\"password\":\"123456\",\"app_key\":\"34D2DEDC049E4D726D83E4E2B1F34D9171\",\"app_type\":\"0\",\"timestamp\":\"1617288677\",\"nonce\":\"123456789\",\"sign\":\"2A71D4F6CD5F52A602A5981E41876D83\"}', '0.0.0.0', '', 0, 1654830032, 1654830032);
INSERT INTO `cnpscy_admin_login_logs_2022_06` (`log_id`, `admin_id`, `log_status`, `log_description`, `request_url`, `log_method`, `request_data`, `created_ip`, `browser_type`, `is_delete`, `created_time`, `updated_time`) VALUES (4, 1, 1, '登录成功', '/admin/auth/login', 'POST', '{\"admin_name\":\"admin\",\"password\":\"123456\",\"app_key\":\"34D2DEDC049E4D726D83E4E2B1F34D9171\",\"app_type\":\"0\",\"timestamp\":\"1617288677\",\"nonce\":\"123456789\",\"sign\":\"2A71D4F6CD5F52A602A5981E41876D83\"}', '0.0.0.0', '', 0, 1654841644, 1654841644);
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_admin_login_logs_2022_07
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_admin_login_logs_2022_07`;
CREATE TABLE `cnpscy_admin_login_logs_2022_07` (
  `log_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '管理员登录日志',
  `admin_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Id',
  `log_status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '登录状态：1：成功；0：失败',
  `log_description` varchar(200) NOT NULL DEFAULT '' COMMENT '描述',
  `request_url` varchar(200) NOT NULL DEFAULT '' COMMENT '请求方法/路由',
  `log_method` varchar(20) NOT NULL DEFAULT '' COMMENT '请求类型/请求方式',
  `request_data` varchar(500) NOT NULL DEFAULT '' COMMENT '请求参数',
  `created_ip` varchar(20) NOT NULL DEFAULT '' COMMENT '创建时的IP',
  `browser_type` varchar(200) NOT NULL DEFAULT '' COMMENT '创建时浏览器类型',
  `is_delete` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除：1：删除；0：正常',
  `created_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`log_id`),
  KEY `admin_login_logs_admin_id_index` (`admin_id`),
  KEY `admin_login_logs_log_status_index` (`log_status`),
  KEY `admin_login_logs_is_delete_index` (`is_delete`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cnpscy_admin_login_logs_2022_07
-- ----------------------------
BEGIN;
INSERT INTO `cnpscy_admin_login_logs_2022_07` (`log_id`, `admin_id`, `log_status`, `log_description`, `request_url`, `log_method`, `request_data`, `created_ip`, `browser_type`, `is_delete`, `created_time`, `updated_time`) VALUES (1, 1, 1, '登录成功', '/admin/auth/login', 'POST', '{\"admin_name\":\"admin\",\"password\":\"123456\",\"app_key\":\"34D2DEDC049E4D726D83E4E2B1F34D9171\",\"app_type\":\"0\",\"timestamp\":\"1617288677\",\"nonce\":\"123456789\",\"sign\":\"2A71D4F6CD5F52A602A5981E41876D83\"}', '0.0.0.0', '', 0, 1657275957, 1657275957);
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_admin_logs
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_admin_logs`;
CREATE TABLE `cnpscy_admin_logs` (
  `log_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '管理员登录日志',
  `admin_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Id',
  `log_description` varchar(200) NOT NULL DEFAULT '' COMMENT '描述',
  `request_url` varchar(200) NOT NULL DEFAULT '' COMMENT '请求方法/路由',
  `log_method` varchar(20) NOT NULL DEFAULT '' COMMENT '请求类型/请求方式',
  `request_data` varchar(500) NOT NULL DEFAULT '' COMMENT '请求参数',
  `created_ip` varchar(20) NOT NULL DEFAULT '' COMMENT '创建时的IP',
  `browser_type` varchar(200) NOT NULL DEFAULT '' COMMENT '创建时浏览器类型',
  `log_duration` varchar(200) NOT NULL DEFAULT '0' COMMENT '请求时长（s）',
  `is_delete` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除：1：删除；0：正常',
  `created_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`log_id`),
  KEY `admin_logs_admin_id_index` (`admin_id`),
  KEY `admin_logs_is_delete_index` (`is_delete`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cnpscy_admin_logs
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_admin_logs_2022_06
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_admin_logs_2022_06`;
CREATE TABLE `cnpscy_admin_logs_2022_06` (
  `log_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '管理员登录日志',
  `admin_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Id',
  `log_description` varchar(200) NOT NULL DEFAULT '' COMMENT '描述',
  `request_url` varchar(200) NOT NULL DEFAULT '' COMMENT '请求方法/路由',
  `log_method` varchar(20) NOT NULL DEFAULT '' COMMENT '请求类型/请求方式',
  `request_data` varchar(500) NOT NULL DEFAULT '' COMMENT '请求参数',
  `created_ip` varchar(20) NOT NULL DEFAULT '' COMMENT '创建时的IP',
  `browser_type` varchar(200) NOT NULL DEFAULT '' COMMENT '创建时浏览器类型',
  `log_duration` varchar(200) NOT NULL DEFAULT '0' COMMENT '请求时长（s）',
  `is_delete` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除：1：删除；0：正常',
  `created_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`log_id`),
  KEY `admin_logs_admin_id_index` (`admin_id`),
  KEY `admin_logs_is_delete_index` (`is_delete`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cnpscy_admin_logs_2022_06
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_admin_logs_2022_07
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_admin_logs_2022_07`;
CREATE TABLE `cnpscy_admin_logs_2022_07` (
  `log_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '管理员登录日志',
  `admin_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Id',
  `log_description` varchar(200) NOT NULL DEFAULT '' COMMENT '描述',
  `request_url` varchar(200) NOT NULL DEFAULT '' COMMENT '请求方法/路由',
  `log_method` varchar(20) NOT NULL DEFAULT '' COMMENT '请求类型/请求方式',
  `request_data` varchar(500) NOT NULL DEFAULT '' COMMENT '请求参数',
  `created_ip` varchar(20) NOT NULL DEFAULT '' COMMENT '创建时的IP',
  `browser_type` varchar(200) NOT NULL DEFAULT '' COMMENT '创建时浏览器类型',
  `log_duration` varchar(200) NOT NULL DEFAULT '0' COMMENT '请求时长（s）',
  `is_delete` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除：1：删除；0：正常',
  `created_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`log_id`),
  KEY `admin_logs_admin_id_index` (`admin_id`),
  KEY `admin_logs_is_delete_index` (`is_delete`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cnpscy_admin_logs_2022_07
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_admin_menus
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_admin_menus`;
CREATE TABLE `cnpscy_admin_menus` (
  `menu_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '菜单栏目表',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '父级id',
  `menu_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '栏目名称',
  `vue_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `vue_path` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'VUE路由路径',
  `vue_redirect` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'Vue的redirect',
  `vue_icon` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '图标',
  `vue_component` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'VUE文件路径',
  `vue_meta` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `external_links` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '外链',
  `api_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '接口路由',
  `api_method` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '接口的请求方式',
  `menu_sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `is_hidden` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否隐藏菜单栏：1：是；0：否',
  `is_check` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否可用：1：可用；0：禁用',
  `is_delete` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除：1：删除；0：正常',
  `created_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`menu_id`) USING BTREE,
  KEY `admin_menus_parent_id_index` (`parent_id`) USING BTREE,
  KEY `admin_menus_is_check_index` (`is_check`) USING BTREE,
  KEY `admin_menus_is_delete_index` (`is_delete`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of cnpscy_admin_menus
-- ----------------------------
BEGIN;
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (1, 0, '首页', 'Dashboard', '/', '/dashboard', 'fa fa-dashboard', 'Layout', '', '', '', '', 1, 0, 1, 1, 1601434917, 1621561441);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (2, 0, '权限管理', 'permissionManage', '/rabc', '/rabc/menus', 'el-icon-lock', 'Layout', '', '', '', '', 2, 0, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (3, 0, '系统设置', 'systemSettings', '', '/configs', 'el-icon-setting', 'Layout', '', '', '', '', 3, 0, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (4, 0, '文章管理', 'articleManage', '/articles', '', 'el-icon-tickets', 'Layout', '', '', '', '', 4, 0, 1, 0, 1609127616, 1621560264);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (5, 1, 'Dashboard', 'Dashboard', 'dashboard', '', '', 'dashboard/index', '', '', 'index', 'GET', 1, 0, 1, 1, 1601434917, 1601437498);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (6, 2, '菜单管理', 'menuManage', 'menus', '', '', 'admin_menus/index', '', '', 'admin/admin_menus', 'GET', 1, 0, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (7, 2, '角色管理', 'roleManage', 'roles', '', '', 'admin_roles/index', '', '', 'admin/admin_roles', 'GET', 2, 0, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (8, 2, '管理员管理', 'adminManage', 'admins', '', '', 'admins/index', '', '', 'admin/admins', 'GET', 3, 0, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (9, 3, '配置管理', 'configManage', '/configs', '', '', 'configs/index', '', '', 'admin/configs', 'GET', 1, 0, 1, 0, 1609126826, 1609126826);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (10, 3, '友情链接', 'friendlinks', '/friendlinks', '', '', 'friendlinks/index', '', '', 'admin/friendlinks', 'GET', 2, 0, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (11, 3, 'Banner管理', 'bannerManage', '/banners', '', '', 'banners/index', '', '', 'admin/banners', 'GET', 3, 0, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (12, 4, '文章分类', 'articleCategory', 'categories', '', '', 'article_categories/index', '', '', 'admin/article_categories', 'GET', 1, 0, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (13, 4, '文章列表', 'articleLists', '', '', '', 'articles/index', '', '', 'admin/articles', 'GET', 2, 0, 1, 0, 1609126826, 1609317417);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (14, 6, '下拉列表', '', '', '', '', '', '', '', 'admin/admin_menus/getSelectLists', 'GET', 5, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (15, 6, '新增', '', '', '', '', '', '', '', 'admin/admin_menus/create', 'POST', 1, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (16, 6, '更新', '', '', '', '', '', '', '', 'admin/admin_menus/update', 'PUT', 3, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (17, 6, '删除', '', '', '', '', '', '', '', 'admin/admin_menus/delete', 'DELETE', 4, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (18, 6, '详情', '', '', '', '', '', '', '', 'admin/admin_menus/detail', 'GET', 2, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (19, 7, '新增', '', '', '', '', '', '', '', 'admin/admin_roles/create', 'POST', 1, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (20, 7, '详情', '', '', '', '', '', '', '', 'admin/admin_roles/detail', 'GET', 1, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (21, 7, '更新', '', '', '', '', '', '', '', 'admin/admin_roles/update', 'PUT', 1, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (22, 7, '删除', '', '', '', '', '', '', '', 'admin/admin_roles/delete', 'DELETE', 1, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (23, 7, '下拉列表', '', '', '', '', '', '', '', 'admin/admin_roles/getSelectLists', 'GET', 1, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (24, 8, '新增', '', '', '', '', '', '', '', 'admin/admins/create', 'POST', 1, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (25, 8, '详情', '', '', '', '', '', '', '', 'admin/admins/detail', 'GET', 2, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (26, 8, '更新', '', '', '', '', '', '', '', 'admin/admins/update', 'PUT', 3, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (27, 8, '删除', '', '', '', '', '', '', '', 'admin/admins/delete', 'DELETE', 4, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (28, 8, '下拉列表', '', '', '', '', '', '', '', 'admin/admins/getSelectLists', 'GET', 5, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (29, 9, '新增', '', '', '', '', '', '', '', 'admin/configs/create', 'POST', 1, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (30, 3, '配置详情（禁止修改父级，VUE的路由层级嵌套问题）', '详情', '/configs/detail', '', '', 'configs/detail', '', '', 'admin/configs/detail', 'GET', 2, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (31, 9, '更新', '', '', '', '', '', '', '', 'admin/configs/update', 'PUT', 3, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (32, 9, '删除', '', '', '', '', '', '', '', 'admin/configs/delete', 'DELETE', 4, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (33, 9, '下拉列表', '', '', '', '', '', '', '', 'admin/configs/getSelectLists', 'GET', 5, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (34, 10, '新增', '', '', '', '', '', '', '', 'admin/friendlinks/create', 'POST', 1, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (35, 10, '详情', '', '', '', '', '', '', '', 'admin/friendlinks/detail', 'GET', 2, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (36, 10, '更新', '', '', '', '', '', '', '', 'admin/friendlinks/update', 'PUT', 3, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (37, 10, '删除', '', '', '', '', '', '', '', 'admin/friendlinks/delete', 'DELETE', 4, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (38, 10, '下拉列表', '', '', '', '', '', '', '', 'admin/friendlinks/getSelectLists', 'GET', 5, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (39, 11, '新增', '', '', '', '', '', '', '', 'admin/banners/create', 'POST', 1, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (40, 11, '详情', '', '', '', '', '', '', '', 'admin/banners/detail', 'GET', 2, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (41, 11, '更新', '', '', '', '', '', '', '', 'admin/banners/update', 'PUT', 3, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (42, 11, '删除', '', '', '', '', '', '', '', 'admin/banners/delete', 'DELETE', 4, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (43, 11, '下拉列表', '', '', '', '', '', '', '', 'admin/banners/getSelectLists', 'GET', 5, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (44, 12, '新增', '', '', '', '', '', '', '', 'admin/article_categories/create', 'POST', 1, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (45, 12, '详情', '', '', '', '', '', '', '', 'admin/article_categories/detail', 'GET', 2, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (46, 12, '更新', '', '', '', '', '', '', '', 'admin/article_categories/update', 'PUT', 3, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (47, 12, '删除', '', '', '', '', '', '', '', 'admin/article_categories/delete', 'DELETE', 4, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (48, 12, '下拉列表', '', '', '', '', '', '', '', 'admin/article_categories/getSelectLists', 'GET', 5, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (49, 13, '新增', '', '', '', '', '', '', '', 'admin/articles/create', 'POST', 1, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (50, 4, '文章详情（禁止修改父级，VUE的路由层级嵌套问题）', '', 'detail', '', '', 'articles/detail', '', '', 'admin/articles/detail', 'GET', 4, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (51, 13, '更新', '', '', '', '', '', '', '', 'admin/articles/update', 'PUT', 3, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (52, 13, '删除', '', '', '', '', '', '', '', 'admin/articles/delete', 'DELETE', 4, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (53, 13, '下拉列表', '', '', '', '', '', '', '', 'admin/articles/getSelectLists', 'GET', 5, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (54, 0, '日志管理', 'logManage', '/log', 'adminloginlogs', 'el-icon-s-order', 'Layout', '', '', '', '', 5, 0, 1, 0, 1609126942, 1609126965);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (55, 54, '管理员登录日志', 'adminLoginLog', 'adminloginlogs', '', '', 'adminloginlogs/index', '', '', 'admin/adminloginlogs', 'GET', 1, 0, 1, 0, 1609127601, 1609127601);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (56, 54, '管理员操作日志', 'adminOperationLog', 'adminlogs', '', '', 'adminlogs/index', '', '', 'admin/adminlogs', 'GET', 2, 0, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (57, 7, '状态变更', '', '', '', '', '', '', '', 'admin/admin_roles/changeFiledStatus', 'PUT', 5, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (58, 6, '状态变更', '', '', '', '', '', '', '', 'admin/admin_menus/changeFiledStatus', 'PUT', 5, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (59, 8, '状态变更', '', '', '', '', '', '', '', 'admin/admins/changeFiledStatus', 'PUT', 5, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (60, 55, '删除', '', '', '', '', '', '', '', 'admin/adminloginlogs/delete', 'DELETE', 1, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (61, 56, '删除', '', '', '', '', '', '', '', 'admin/adminlogs/delete', 'DELETE', 1, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (62, 12, '状态变更', '', '', '', '', '', '', '', 'admin/article_categories/changeFiledStatus', 'PUT', 5, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (63, 11, '状态变更', '', '', '', '', '', '', '', 'admin/banners/changeFiledStatus', 'PUT', 5, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (64, 10, '状态变更', '', '', '', '', '', '', '', 'admin/friendlinks/changeFiledStatus', 'PUT', 5, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (65, 9, '状态变更', '', '', '', '', '', '', '', 'admin/configs/changeFiledStatus', 'PUT', 5, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (66, 13, '状态变更', '', '', '', '', '', '', '', 'admin/articles/changeFiledStatus', 'PUT', 5, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (67, 3, '版本管理', 'versionManage', 'versions', '', '', 'versions/index', '', '', 'admin/versions', '', 4, 0, 1, 0, 1610001528, 1610001555);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (68, 67, '新增', '', '', '', '', '', '', '', 'admin/versions/create', 'POST', 1, 1, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (69, 67, '详情', '', '', '', '', '', '', '', 'admin/versions/detail', 'GET', 2, 1, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (70, 67, '更新', '', '', '', '', '', '', '', 'admin/versions/update', 'PUT', 3, 1, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (71, 67, '删除', '', '', '', '', '', '', '', 'admin/versions/delete', 'DELETE', 4, 1, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (72, 4, '文章标签', 'articleLabels', 'labels', '', '', 'article_labels/index', '', '', 'admin/article_labels', 'GET', 3, 0, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (73, 72, '新增', '', '', '', '', '', '', '', 'admin/article_labels/create', 'POST', 0, 0, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (74, 72, '详情', '', '', '', '', '', '', '', 'admin/article_labels/detail', 'GET', 0, 0, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (75, 72, '更新', '', '', '', '', '', '', '', 'admin/article_labels/update', 'PUT', 0, 0, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (76, 72, '删除', '', '', '', '', '', '', '', 'admin/article_labels/delete', 'DELETE', 0, 0, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (77, 3, '数据库管理', 'databaseManage', 'databases', '', '', 'databases/index', '', '', 'databases', 'GET', 5, 0, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus` (`menu_id`, `parent_id`, `menu_name`, `vue_name`, `vue_path`, `vue_redirect`, `vue_icon`, `vue_component`, `vue_meta`, `external_links`, `api_url`, `api_method`, `menu_sort`, `is_hidden`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (78, 3, '备份管理', 'backupsManage', 'backups', '', '', 'databases/backups', '', '', 'databases/backups', 'GET', 6, 0, 1, 0, 0, 0);
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_admin_roles
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_admin_roles`;
CREATE TABLE `cnpscy_admin_roles` (
  `role_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '角色表',
  `role_name` varchar(100) NOT NULL DEFAULT '' COMMENT '角色名称',
  `role_remarks` varchar(256) NOT NULL DEFAULT '' COMMENT '备注',
  `is_check` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否可用：1：可用；0：禁用',
  `is_delete` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除',
  `created_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`role_id`) USING BTREE,
  KEY `blog_admin_roles_created_time_index` (`created_time`) USING BTREE,
  KEY `blog_admin_roles_is_delete_index` (`is_delete`) USING BTREE,
  KEY `blog_admin_roles_is_check_index` (`is_check`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of cnpscy_admin_roles
-- ----------------------------
BEGIN;
INSERT INTO `cnpscy_admin_roles` (`role_id`, `role_name`, `role_remarks`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (1, '超级管理员', '权限最大的人', 1, 0, 1577671704, 1601293601);
INSERT INTO `cnpscy_admin_roles` (`role_id`, `role_name`, `role_remarks`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (2, 'test', '备注信息', 0, 0, 1598348975, 1609318749);
INSERT INTO `cnpscy_admin_roles` (`role_id`, `role_name`, `role_remarks`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (3, '111', '2121', 0, 1, 1608795030, 1609827748);
INSERT INTO `cnpscy_admin_roles` (`role_id`, `role_name`, `role_remarks`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (7, '31232', '12321', 0, 0, 1608795106, 1609901582);
INSERT INTO `cnpscy_admin_roles` (`role_id`, `role_name`, `role_remarks`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (10, '42342432', '42423', 1, 0, 1608795181, 1609901579);
INSERT INTO `cnpscy_admin_roles` (`role_id`, `role_name`, `role_remarks`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (11, '3123213', '2132321', 0, 1, 1608796971, 1609817914);
INSERT INTO `cnpscy_admin_roles` (`role_id`, `role_name`, `role_remarks`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (12, '111', '111', 0, 1, 1609828403, 1609830841);
INSERT INTO `cnpscy_admin_roles` (`role_id`, `role_name`, `role_remarks`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (13, '111', '111', 1, 0, 1609831035, 1609831035);
INSERT INTO `cnpscy_admin_roles` (`role_id`, `role_name`, `role_remarks`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (14, '全部权限的角色', '', 1, 1, 1609832355, 1609832468);
INSERT INTO `cnpscy_admin_roles` (`role_id`, `role_name`, `role_remarks`, `is_check`, `is_delete`, `created_time`, `updated_time`) VALUES (15, '全部权限的角色', '', 1, 0, 1609832355, 1609832355);
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_admin_routes
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_admin_routes`;
CREATE TABLE `cnpscy_admin_routes` (
  `route_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '父级Id',
  `route_name` varchar(100) NOT NULL DEFAULT '' COMMENT '路由名称',
  `route_prefix` varchar(100) NOT NULL DEFAULT '' COMMENT '路由前缀',
  `route_url` varchar(100) NOT NULL DEFAULT '' COMMENT '路由地址',
  `route_method` varchar(10) NOT NULL DEFAULT '' COMMENT '路由请求方式',
  `route_controller` varchar(100) NOT NULL DEFAULT '' COMMENT '路由控制器',
  `route_function` varchar(100) NOT NULL DEFAULT '' COMMENT '路由的控制器方法',
  `route_group_middleware` varchar(100) NOT NULL DEFAULT '' COMMENT '路由分组中间件（所有下级）',
  `route_middleware` varchar(100) NOT NULL DEFAULT '' COMMENT '路由中间件',
  `is_delete` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除：1：删除；0：正常',
  `created_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`route_id`),
  KEY `admin_routes_parent_id_index` (`parent_id`),
  KEY `admin_routes_is_delete_index` (`is_delete`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='后台API路由表';

-- ----------------------------
-- Records of cnpscy_admin_routes
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_admin_with_roles
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_admin_with_roles`;
CREATE TABLE `cnpscy_admin_with_roles` (
  `with_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '角色Id',
  `admin_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '管理员Id',
  PRIMARY KEY (`with_id`) USING BTREE,
  KEY `role_id` (`role_id`) USING BTREE,
  KEY `admin_id` (`admin_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='后台-角色与管理员关联表';

-- ----------------------------
-- Records of cnpscy_admin_with_roles
-- ----------------------------
BEGIN;
INSERT INTO `cnpscy_admin_with_roles` (`with_id`, `role_id`, `admin_id`) VALUES (1, 1, 2);
INSERT INTO `cnpscy_admin_with_roles` (`with_id`, `role_id`, `admin_id`) VALUES (3, 2, 102);
INSERT INTO `cnpscy_admin_with_roles` (`with_id`, `role_id`, `admin_id`) VALUES (6, 1, 107);
INSERT INTO `cnpscy_admin_with_roles` (`with_id`, `role_id`, `admin_id`) VALUES (9, 1, 119);
INSERT INTO `cnpscy_admin_with_roles` (`with_id`, `role_id`, `admin_id`) VALUES (11, 3, 119);
INSERT INTO `cnpscy_admin_with_roles` (`with_id`, `role_id`, `admin_id`) VALUES (12, 11, 119);
INSERT INTO `cnpscy_admin_with_roles` (`with_id`, `role_id`, `admin_id`) VALUES (14, 1, 123);
INSERT INTO `cnpscy_admin_with_roles` (`with_id`, `role_id`, `admin_id`) VALUES (15, 10, 123);
INSERT INTO `cnpscy_admin_with_roles` (`with_id`, `role_id`, `admin_id`) VALUES (16, 11, 123);
INSERT INTO `cnpscy_admin_with_roles` (`with_id`, `role_id`, `admin_id`) VALUES (17, 1, 1);
INSERT INTO `cnpscy_admin_with_roles` (`with_id`, `role_id`, `admin_id`) VALUES (18, 11, 1);
INSERT INTO `cnpscy_admin_with_roles` (`with_id`, `role_id`, `admin_id`) VALUES (20, 3, 123);
INSERT INTO `cnpscy_admin_with_roles` (`with_id`, `role_id`, `admin_id`) VALUES (21, 7, 123);
INSERT INTO `cnpscy_admin_with_roles` (`with_id`, `role_id`, `admin_id`) VALUES (22, 2, 123);
INSERT INTO `cnpscy_admin_with_roles` (`with_id`, `role_id`, `admin_id`) VALUES (23, 2, 118);
INSERT INTO `cnpscy_admin_with_roles` (`with_id`, `role_id`, `admin_id`) VALUES (24, 7, 118);
INSERT INTO `cnpscy_admin_with_roles` (`with_id`, `role_id`, `admin_id`) VALUES (25, 10, 118);
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_admins
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_admins`;
CREATE TABLE `cnpscy_admins` (
  `admin_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '管理员表',
  `admin_name` varchar(100) NOT NULL DEFAULT '' COMMENT '管理员',
  `admin_email` varchar(100) NOT NULL DEFAULT '' COMMENT '邮箱',
  `admin_head` varchar(200) NOT NULL DEFAULT '' COMMENT '头像',
  `password` varchar(60) NOT NULL DEFAULT '' COMMENT '密码',
  `is_check` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否审核：0：审核中；1：正常；2：禁用',
  `is_delete` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除：1：删除；0：正常',
  `kick_out` tinyint(1) unsigned NOT NULL DEFAULT '2' COMMENT '是否踢出登录：0：表示在线；1：踢出登录；2.未登录',
  `use_role` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '正在使用的角色Id',
  PRIMARY KEY (`admin_id`),
  KEY `admins_kick_out_index` (`kick_out`),
  KEY `admins_is_check_index` (`is_check`),
  KEY `admins_is_delete_index` (`is_delete`)
) ENGINE=InnoDB AUTO_INCREMENT=124 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cnpscy_admins
-- ----------------------------
BEGIN;
INSERT INTO `cnpscy_admins` (`admin_id`, `admin_name`, `admin_email`, `admin_head`, `password`, `is_check`, `is_delete`, `kick_out`, `use_role`) VALUES (1, 'admin', '$2y$10$HyOTnVzzX3RhoCXlQ88qfumA81elPAPnBaORTWBSZT8Xg0s68ttKm', '202012/2r8hEvdSVBQa1s3sBCdcXt6fI57nVebNoaFaluQK.png', '$2y$10$HyOTnVzzX3RhoCXlQ88qfumA81elPAPnBaORTWBSZT8Xg0s68ttKm', 1, 0, 2, 0);
INSERT INTO `cnpscy_admins` (`admin_id`, `admin_name`, `admin_email`, `admin_head`, `password`, `is_check`, `is_delete`, `kick_out`, `use_role`) VALUES (107, 'test', '$2y$10$gcgnFEWG47jf2JqM8onRp.OJrnB6JiMOP0.pClO0T1MbQPoo7ZB2C', '1', '$2y$10$gcgnFEWG47jf2JqM8onRp.OJrnB6JiMOP0.pClO0T1MbQPoo7ZB2C', 1, 0, 2, 0);
INSERT INTO `cnpscy_admins` (`admin_id`, `admin_name`, `admin_email`, `admin_head`, `password`, `is_check`, `is_delete`, `kick_out`, `use_role`) VALUES (108, 'test1', '$2y$10$t6rC29.BfNtQc.s8DB8UkeNIQ3ZxjOP0u231oiTtiA2qQLdeOuumm', '86', '$2y$10$t6rC29.BfNtQc.s8DB8UkeNIQ3ZxjOP0u231oiTtiA2qQLdeOuumm', 1, 0, 2, 0);
INSERT INTO `cnpscy_admins` (`admin_id`, `admin_name`, `admin_email`, `admin_head`, `password`, `is_check`, `is_delete`, `kick_out`, `use_role`) VALUES (109, 'test1', '$2y$10$t6rC29.BfNtQc.s8DB8UkeNIQ3ZxjOP0u231oiTtiA2qQLdeOuumm', '86', '$2y$10$t6rC29.BfNtQc.s8DB8UkeNIQ3ZxjOP0u231oiTtiA2qQLdeOuumm', 1, 0, 2, 0);
INSERT INTO `cnpscy_admins` (`admin_id`, `admin_name`, `admin_email`, `admin_head`, `password`, `is_check`, `is_delete`, `kick_out`, `use_role`) VALUES (110, 'test1', '$2y$10$t6rC29.BfNtQc.s8DB8UkeNIQ3ZxjOP0u231oiTtiA2qQLdeOuumm', '86', '$2y$10$t6rC29.BfNtQc.s8DB8UkeNIQ3ZxjOP0u231oiTtiA2qQLdeOuumm', 1, 0, 2, 0);
INSERT INTO `cnpscy_admins` (`admin_id`, `admin_name`, `admin_email`, `admin_head`, `password`, `is_check`, `is_delete`, `kick_out`, `use_role`) VALUES (111, 'test1', '$2y$10$t6rC29.BfNtQc.s8DB8UkeNIQ3ZxjOP0u231oiTtiA2qQLdeOuumm', '86', '$2y$10$t6rC29.BfNtQc.s8DB8UkeNIQ3ZxjOP0u231oiTtiA2qQLdeOuumm', 1, 0, 2, 0);
INSERT INTO `cnpscy_admins` (`admin_id`, `admin_name`, `admin_email`, `admin_head`, `password`, `is_check`, `is_delete`, `kick_out`, `use_role`) VALUES (112, 'test1', '$2y$10$t6rC29.BfNtQc.s8DB8UkeNIQ3ZxjOP0u231oiTtiA2qQLdeOuumm', '86', '$2y$10$t6rC29.BfNtQc.s8DB8UkeNIQ3ZxjOP0u231oiTtiA2qQLdeOuumm', 1, 0, 2, 0);
INSERT INTO `cnpscy_admins` (`admin_id`, `admin_name`, `admin_email`, `admin_head`, `password`, `is_check`, `is_delete`, `kick_out`, `use_role`) VALUES (113, 'test', 'test1', '86', 'test1', 1, 0, 2, 0);
INSERT INTO `cnpscy_admins` (`admin_id`, `admin_name`, `admin_email`, `admin_head`, `password`, `is_check`, `is_delete`, `kick_out`, `use_role`) VALUES (114, 'test1', '$2y$10$AeU9vwBx0d4KttM6yKcdWu8mgy2qgcTVK/v.I4MJuEzNlqz.Li9GG', '86', '$2y$10$AeU9vwBx0d4KttM6yKcdWu8mgy2qgcTVK/v.I4MJuEzNlqz.Li9GG', 1, 0, 2, 0);
INSERT INTO `cnpscy_admins` (`admin_id`, `admin_name`, `admin_email`, `admin_head`, `password`, `is_check`, `is_delete`, `kick_out`, `use_role`) VALUES (117, 'test123', '$2y$10$lvVejUPm/lypn8Xr1YNHdOYdy7rrjqiv6DWsYlYAR4R4yidnLBg8e', '1', '$2y$10$lvVejUPm/lypn8Xr1YNHdOYdy7rrjqiv6DWsYlYAR4R4yidnLBg8e', 1, 0, 2, 0);
INSERT INTO `cnpscy_admins` (`admin_id`, `admin_name`, `admin_email`, `admin_head`, `password`, `is_check`, `is_delete`, `kick_out`, `use_role`) VALUES (118, 'test12311111', '$2y$10$5hSULbXycOJGQgU2p9ZGaepyIW9qG5DvG1BvyipESM.8WUf3rNKvy', '1', '$2y$10$5hSULbXycOJGQgU2p9ZGaepyIW9qG5DvG1BvyipESM.8WUf3rNKvy', 1, 0, 2, 0);
INSERT INTO `cnpscy_admins` (`admin_id`, `admin_name`, `admin_email`, `admin_head`, `password`, `is_check`, `is_delete`, `kick_out`, `use_role`) VALUES (119, 'test1232', '$2y$10$tnqyaBCn7HMwuellMuaH2OQq9ax1S1Wmnp7MAtBQ5Tf.dQ6a1fyeu', '1', '$2y$10$tnqyaBCn7HMwuellMuaH2OQq9ax1S1Wmnp7MAtBQ5Tf.dQ6a1fyeu', 1, 1, 2, 0);
INSERT INTO `cnpscy_admins` (`admin_id`, `admin_name`, `admin_email`, `admin_head`, `password`, `is_check`, `is_delete`, `kick_out`, `use_role`) VALUES (123, 'demo', '$2y$10$BSCJxQYvJAvfbQK2uOQNT.0SdzPvWc8GOBl09HADOYzo17hgW/rti', '202012/3yNDfZMiAV0rmOR0mhqkbmk3GWUgSOySgB6OCGVw.png', '$2y$10$BSCJxQYvJAvfbQK2uOQNT.0SdzPvWc8GOBl09HADOYzo17hgW/rti', 1, 1, 2, 0);
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_adverts
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_adverts`;
CREATE TABLE `cnpscy_adverts` (
  `advert_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '广告表',
  `advert_name` varchar(200) NOT NULL DEFAULT '' COMMENT '标题',
  `advert_cover` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '封面',
  `advert_url` varchar(200) NOT NULL DEFAULT '' COMMENT '外链',
  `is_delete` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除：1：删除；0：正常',
  `created_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`advert_id`),
  KEY `adverts_is_delete_index` (`is_delete`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cnpscy_adverts
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_apps
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_apps`;
CREATE TABLE `cnpscy_apps` (
  `app_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `app_name` varchar(100) NOT NULL DEFAULT '' COMMENT '名称',
  `app_key` varchar(100) NOT NULL DEFAULT '' COMMENT 'key',
  `app_secret` varchar(100) NOT NULL DEFAULT '' COMMENT '秘钥',
  `app_type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'APP类型：0.Web；1.Android；2.IOS',
  `is_delete` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除：1：删除；0：正常',
  `created_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`app_id`),
  KEY `apps_app_type_index` (`app_type`),
  KEY `apps_is_delete_index` (`is_delete`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='APP管理表';

-- ----------------------------
-- Records of cnpscy_apps
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_article_categories
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_article_categories`;
CREATE TABLE `cnpscy_article_categories` (
  `category_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '文章分类表',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '父级Id',
  `category_name` varchar(200) NOT NULL DEFAULT '' COMMENT '名称',
  `category_cover` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '封面',
  `category_description` varchar(200) NOT NULL DEFAULT '' COMMENT '描述',
  `category_sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `is_delete` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除：1：删除；0：正常',
  `created_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`category_id`),
  KEY `article_categories_parent_id_index` (`parent_id`),
  KEY `article_categories_is_delete_index` (`is_delete`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cnpscy_article_categories
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_article_contents
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_article_contents`;
CREATE TABLE `cnpscy_article_contents` (
  `article_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文章Id',
  `article_content` text COMMENT '内容',
  KEY `article_contents_article_id_index` (`article_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cnpscy_article_contents
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_articles
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_articles`;
CREATE TABLE `cnpscy_articles` (
  `article_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '文章表',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '会员Id',
  `article_category` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '分类Id',
  `article_title` varchar(200) NOT NULL DEFAULT '' COMMENT '文章标题',
  `article_cover` varchar(200) NOT NULL DEFAULT '' COMMENT '封面',
  `article_keywords` varchar(200) NOT NULL DEFAULT '' COMMENT '关键词',
  `article_description` varchar(200) NOT NULL DEFAULT '' COMMENT '描述',
  `article_sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `set_top` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '置顶',
  `is_recommend` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '推荐',
  `is_public` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否公开：0：私密；1：是；2.密码访问',
  `access_password` varchar(60) NOT NULL DEFAULT '' COMMENT '访问密码',
  `article_origin` varchar(200) NOT NULL DEFAULT '' COMMENT '文章来源',
  `article_author` varchar(200) NOT NULL DEFAULT '' COMMENT '文章作者',
  `article_link` varchar(300) NOT NULL DEFAULT '' COMMENT '详情外链',
  `read_num` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '阅读数量',
  `is_delete` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除：1：删除；0：正常',
  `created_ip` varchar(50) NOT NULL DEFAULT '' COMMENT '创建时的IP',
  `praise_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '点赞数量',
  `collection_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '收藏数量',
  `comment_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '评论数量',
  `created_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`article_id`),
  KEY `articles_user_id_index` (`user_id`),
  KEY `articles_is_public_index` (`is_public`),
  KEY `articles_is_delete_index` (`is_delete`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cnpscy_articles
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_banners
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_banners`;
CREATE TABLE `cnpscy_banners` (
  `banner_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Banner表',
  `banner_name` varchar(100) NOT NULL DEFAULT '' COMMENT '标题',
  `banner_cover` varchar(200) NOT NULL DEFAULT '' COMMENT '封面',
  `banner_link` varchar(200) NOT NULL DEFAULT '' COMMENT '外链',
  `banner_words` varchar(200) NOT NULL DEFAULT '' COMMENT '文字描述',
  `banner_sort` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '排序【升序】',
  `is_check` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否审核：1：正常；0：禁用',
  `is_delete` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除：1：删除；0：正常',
  `created_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`banner_id`),
  KEY `banners_banner_sort_index` (`banner_sort`),
  KEY `banners_is_check_index` (`is_check`),
  KEY `banners_is_delete_index` (`is_delete`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cnpscy_banners
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_bbs_album_users
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_bbs_album_users`;
CREATE TABLE `cnpscy_bbs_album_users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '会员的相册记录表',
  `album_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '相册Id',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '会员Id',
  `image_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '图片Id',
  `created_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `bbs_album_users_album_id_index` (`album_id`),
  KEY `bbs_album_users_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cnpscy_bbs_album_users
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_bbs_albums
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_bbs_albums`;
CREATE TABLE `cnpscy_bbs_albums` (
  `album_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '会员相册表',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '会员Id',
  `album_name` varchar(200) NOT NULL DEFAULT '' COMMENT '名称',
  `album_logo` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '封面',
  `is_public` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否公开：0：否；1：是；2.密码访问',
  `visit_password` varchar(60) NOT NULL DEFAULT '' COMMENT '访问密码',
  `is_delete` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除：1：删除；0：正常',
  `created_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`album_id`),
  KEY `bbs_albums_user_id_index` (`user_id`),
  KEY `bbs_albums_is_public_index` (`is_public`),
  KEY `bbs_albums_is_delete_index` (`is_delete`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cnpscy_bbs_albums
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_bbs_chat_records
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_bbs_chat_records`;
CREATE TABLE `cnpscy_bbs_chat_records` (
  `record_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '会员的聊天记录表',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '会员Id',
  `friend_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '好友Id',
  `friend_type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '好友类型：1：好友；2：临时会话',
  `chat_type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '内容格式：0：文本；1.图片；2.语音；3.视频',
  `chat_content` text COMMENT '聊天内容',
  `is_recall` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否撤回：1：是；0：否',
  `is_read` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否已读：1：是；0：否',
  `is_delete` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除：1：删除；0：正常',
  `created_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`record_id`),
  KEY `bbs_chat_records_user_id_index` (`user_id`),
  KEY `bbs_chat_records_friend_id_index` (`friend_id`),
  KEY `bbs_chat_records_is_delete_index` (`is_delete`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cnpscy_bbs_chat_records
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_bbs_dynamic_collections
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_bbs_dynamic_collections`;
CREATE TABLE `cnpscy_bbs_dynamic_collections` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '会员动态的收藏记录表',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '会员Id',
  `dynamic_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '动态Id',
  `author_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '作者Id',
  `is_read` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否已读：0：否；1：是',
  `created_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `bbs_dynamic_collections_user_id_index` (`user_id`),
  KEY `bbs_dynamic_collections_dynamic_id_index` (`dynamic_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cnpscy_bbs_dynamic_collections
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_bbs_dynamic_comments
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_bbs_dynamic_comments`;
CREATE TABLE `cnpscy_bbs_dynamic_comments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '会员动态的评论记录表',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '会员Id',
  `dynamic_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '动态Id',
  `top_level` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '顶级的Id（顶级上一级的reply_id = 0）',
  `reply_user` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '回复会员Id',
  `reply_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '回复的动态Id',
  `comment_content` varchar(1000) NOT NULL DEFAULT '' COMMENT '内容',
  `author_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '作者Id',
  `is_read` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否已读：0：否；1：是',
  `created_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `bbs_dynamic_comments_user_id_index` (`user_id`),
  KEY `bbs_dynamic_comments_dynamic_id_index` (`dynamic_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cnpscy_bbs_dynamic_comments
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_bbs_dynamic_praises
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_bbs_dynamic_praises`;
CREATE TABLE `cnpscy_bbs_dynamic_praises` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '会员动态的点赞记录表',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '会员Id',
  `dynamic_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '动态Id',
  `author_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '作者Id',
  `is_read` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否已读：0：否；1：是',
  `created_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `bbs_dynamic_praises_user_id_index` (`user_id`),
  KEY `bbs_dynamic_praises_is_read_index` (`is_read`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cnpscy_bbs_dynamic_praises
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_bbs_dynamics
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_bbs_dynamics`;
CREATE TABLE `cnpscy_bbs_dynamics` (
  `dynamic_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '会员动态表',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '会员Id',
  `dynamic_content` varchar(1000) NOT NULL DEFAULT '' COMMENT '内容',
  `dynamic_file` varchar(1000) NOT NULL DEFAULT '' COMMENT '素材',
  `file_type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '素材类型：0.无；1.图片；2.视频',
  `praise_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '点赞数量',
  `comment_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '评论数量',
  `collection_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '收藏数量',
  `is_public` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否公开：0：否；1：是',
  `is_delete` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除：1：删除；0：正常',
  `created_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`dynamic_id`),
  KEY `bbs_dynamics_user_id_index` (`user_id`),
  KEY `bbs_dynamics_is_public_index` (`is_public`),
  KEY `bbs_dynamics_is_delete_index` (`is_delete`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cnpscy_bbs_dynamics
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_bbs_friend_applies
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_bbs_friend_applies`;
CREATE TABLE `cnpscy_bbs_friend_applies` (
  `apply_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '好友申请表',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '会员Id',
  `friend_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '申请人/好友Id',
  `apply_remark` varchar(200) NOT NULL DEFAULT '' COMMENT '申请备注',
  `user_reason` varchar(200) NOT NULL DEFAULT '' COMMENT '回应',
  `is_check` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态：0.待审核；1.已同意；2.已拒绝',
  `created_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`apply_id`),
  KEY `bbs_friend_applies_user_id_index` (`user_id`),
  KEY `bbs_friend_applies_is_check_index` (`is_check`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cnpscy_bbs_friend_applies
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_bbs_friend_groups
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_bbs_friend_groups`;
CREATE TABLE `cnpscy_bbs_friend_groups` (
  `group_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '好友分组表',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '会员Id',
  `group_name` varchar(100) NOT NULL DEFAULT '' COMMENT '名称',
  `friend_nums` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '好友数量',
  `created_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`group_id`),
  KEY `bbs_friend_groups_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cnpscy_bbs_friend_groups
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_bbs_friends
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_bbs_friends`;
CREATE TABLE `cnpscy_bbs_friends` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '会员好友表',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '会员Id',
  `friend_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '好友Id',
  `group_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '好友分组',
  `is_special` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '特别关注',
  `is_blacklist` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否拉黑：1：是；0：否',
  `friend_remarks` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  `created_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `bbs_friends_user_id_index` (`user_id`),
  KEY `bbs_friends_is_special_index` (`is_special`),
  KEY `bbs_friends_group_id_index` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cnpscy_bbs_friends
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_bbs_group_chats
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_bbs_group_chats`;
CREATE TABLE `cnpscy_bbs_group_chats` (
  `record_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '群聊的聊天记录表',
  `group_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '群聊Id',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '会员Id',
  `chat_content` text COMMENT '内容',
  `chat_type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '内容格式：0：文本；1.图片；2.语音；3.视频',
  `is_recall` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否撤回：1：是；0：否',
  `is_delete` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除：1：删除；0：正常',
  `created_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`record_id`),
  KEY `bbs_group_chats_group_id_index` (`group_id`),
  KEY `bbs_group_chats_user_id_index` (`user_id`),
  KEY `bbs_group_chats_is_delete_index` (`is_delete`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cnpscy_bbs_group_chats
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_bbs_group_notices
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_bbs_group_notices`;
CREATE TABLE `cnpscy_bbs_group_notices` (
  `notice_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '群公告表',
  `group_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '群聊Id',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '发布者',
  `notice_content` text COMMENT '公告内容',
  `is_delete` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除：1：删除；0：正常',
  `created_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`notice_id`),
  KEY `bbs_group_notices_group_id_index` (`group_id`),
  KEY `bbs_group_notices_is_delete_index` (`is_delete`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cnpscy_bbs_group_notices
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_bbs_group_users
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_bbs_group_users`;
CREATE TABLE `cnpscy_bbs_group_users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '群聊的成员表',
  `group_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '群聊Id',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '会员Id',
  `card_name` varchar(200) NOT NULL DEFAULT '' COMMENT '群名片',
  `is_admin` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否管理员：0：普通；1：管理员；2.群主',
  `is_delete` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除：1：删除；0：正常',
  `created_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `bbs_group_users_group_id_index` (`group_id`),
  KEY `bbs_group_users_is_delete_index` (`is_delete`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cnpscy_bbs_group_users
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_bbs_groups
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_bbs_groups`;
CREATE TABLE `cnpscy_bbs_groups` (
  `group_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '群聊/群组表',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建者',
  `group_name` varchar(200) NOT NULL DEFAULT '' COMMENT '群组名称',
  `group_introduce` varchar(400) NOT NULL DEFAULT '' COMMENT '群介绍',
  `group_cover` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '封面',
  `group_users` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '人数',
  `is_delete` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除：1：删除；0：正常',
  `created_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`group_id`),
  KEY `bbs_groups_user_id_index` (`user_id`),
  KEY `bbs_groups_is_delete_index` (`is_delete`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cnpscy_bbs_groups
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_bbs_guestbooks
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_bbs_guestbooks`;
CREATE TABLE `cnpscy_bbs_guestbooks` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '会员留言表',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '会员Id',
  `commenter_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '留言会员的Id',
  `book_content` varchar(1000) NOT NULL DEFAULT '' COMMENT '内容',
  `reply_content` varchar(1000) NOT NULL DEFAULT '' COMMENT '回复内容',
  `created_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `bbs_guestbooks_user_id_index` (`user_id`),
  KEY `bbs_guestbooks_commenter_id_index` (`commenter_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cnpscy_bbs_guestbooks
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_bbs_user_experience_records
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_bbs_user_experience_records`;
CREATE TABLE `cnpscy_bbs_user_experience_records` (
  `record_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '会员经验变动记录表',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '会员Id',
  `experience_num` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '获得多少经验',
  `get_type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '获得类型：\n                0：会员登录【一次 + 1】；\n                1.签到【累加】；\n                2.支付【一次 + 5】；\n                3.绑定手机号码【 + 10 】；\n                4.验证邮箱【 + 10 】；\n                5.实名认证【 + 20 】',
  `description` varchar(256) NOT NULL DEFAULT '' COMMENT '描述',
  `created_ip` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时的IP-ip2long转换',
  `browser_type` varchar(256) NOT NULL DEFAULT '' COMMENT '创建时浏览器类型',
  `is_delete` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除：1：删除；0：正常',
  `created_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`record_id`),
  KEY `bbs_user_experience_records_user_id_index` (`user_id`),
  KEY `bbs_user_experience_records_get_type_index` (`get_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cnpscy_bbs_user_experience_records
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_bbs_user_grades
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_bbs_user_grades`;
CREATE TABLE `cnpscy_bbs_user_grades` (
  `grade_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '会员等级表【禁止删除，允许修改】',
  `grade_name` varchar(256) NOT NULL DEFAULT '' COMMENT '等级名称',
  `need_value` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '至少需要的经验值',
  `created_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `is_delete` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除：1：删除；0：正常',
  PRIMARY KEY (`grade_id`),
  KEY `bbs_user_grades_is_delete_index` (`is_delete`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cnpscy_bbs_user_grades
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_bbs_user_settings
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_bbs_user_settings`;
CREATE TABLE `cnpscy_bbs_user_settings` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cnpscy_bbs_user_settings
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_bbs_user_signs
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_bbs_user_signs`;
CREATE TABLE `cnpscy_bbs_user_signs` (
  `sign_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '签到记录表',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '会员Id',
  `integral_reward` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '签到:积分奖励',
  `sign_type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '签到状态：0.签到；1.补签；2.管理员签到',
  `description` varchar(256) NOT NULL DEFAULT '' COMMENT '描述',
  `created_ip` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时的IP-ip2long转换',
  `browser_type` varchar(256) NOT NULL DEFAULT '' COMMENT '创建时浏览器类型',
  `is_delete` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除：1：删除；0：正常',
  `created_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`sign_id`),
  KEY `bbs_user_signs_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cnpscy_bbs_user_signs
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_configs
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_configs`;
CREATE TABLE `cnpscy_configs` (
  `config_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '系统参数配置表',
  `config_title` varchar(200) NOT NULL DEFAULT '' COMMENT '标题',
  `config_name` varchar(200) NOT NULL DEFAULT '' COMMENT '参数名',
  `config_value` varchar(200) NOT NULL DEFAULT '' COMMENT '参数值',
  `config_group` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '分组',
  `config_type` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '类型',
  `config_sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `config_extra` varchar(300) NOT NULL DEFAULT '' COMMENT '配置项',
  `config_remark` varchar(300) NOT NULL DEFAULT '' COMMENT '说明',
  `is_check` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否启用：1：是；0：否',
  `is_delete` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除：1：删除；0：正常',
  `created_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`config_id`),
  KEY `configs_config_group_index` (`config_group`),
  KEY `configs_is_check_index` (`is_check`),
  KEY `configs_is_delete_index` (`is_delete`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cnpscy_configs
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_friendlinks
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_friendlinks`;
CREATE TABLE `cnpscy_friendlinks` (
  `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '友情链接表',
  `link_name` varchar(100) NOT NULL DEFAULT '' COMMENT '外链名称',
  `link_url` varchar(200) NOT NULL DEFAULT '' COMMENT '外链URL',
  `link_cover` varchar(200) NOT NULL DEFAULT '' COMMENT '封面',
  `link_sort` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '排序【升序】',
  `open_window` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否打开新窗口：1：是；0：否',
  `is_check` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否审核：1：正常；0：禁用',
  `is_delete` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除：1：删除；0：正常',
  `created_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`link_id`),
  KEY `friendlinks_link_sort_index` (`link_sort`),
  KEY `friendlinks_is_check_index` (`is_check`),
  KEY `friendlinks_is_delete_index` (`is_delete`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cnpscy_friendlinks
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_galleries
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_galleries`;
CREATE TABLE `cnpscy_galleries` (
  `gallery_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '图库表',
  `gallery_name` varchar(200) NOT NULL DEFAULT '' COMMENT '图库名称',
  `gallery_cover` varchar(200) NOT NULL DEFAULT '' COMMENT '封面',
  `gallery_origin` varchar(200) NOT NULL DEFAULT '' COMMENT '来源链接',
  `total_num` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '图片总量',
  `is_delete` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除：1：删除；0：正常',
  `created_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`gallery_id`),
  KEY `galleries_is_delete_index` (`is_delete`),
  KEY `galleries_created_time_index` (`created_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cnpscy_galleries
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_gallery_details
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_gallery_details`;
CREATE TABLE `cnpscy_gallery_details` (
  `detail_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '图库详情表',
  `gallery_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '图片库Id',
  `gallery_describe` varchar(300) DEFAULT NULL COMMENT '描述',
  `gallery_pictures` text COMMENT '图片列表（JSON）',
  `picture_nums` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '图片数量',
  `origin_link` varchar(200) NOT NULL DEFAULT '' COMMENT '来源链接',
  `is_delete` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除：1：删除；0：正常',
  `created_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`detail_id`),
  KEY `gallery_details_gallery_id_index` (`gallery_id`),
  KEY `gallery_details_is_delete_index` (`is_delete`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cnpscy_gallery_details
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_gallery_tags
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_gallery_tags`;
CREATE TABLE `cnpscy_gallery_tags` (
  `tag_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '图库标签表',
  `tag_name` varchar(200) NOT NULL DEFAULT '' COMMENT '图库标签名称',
  `origin_link` varchar(200) NOT NULL DEFAULT '' COMMENT '来源链接',
  `is_delete` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除：1：删除；0：正常',
  `created_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`tag_id`),
  KEY `gallery_tags_is_delete_index` (`is_delete`),
  KEY `gallery_tags_created_time_index` (`created_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cnpscy_gallery_tags
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_gallery_with_tags
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_gallery_with_tags`;
CREATE TABLE `cnpscy_gallery_with_tags` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '图库与标签关联表',
  `tag_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '标签Id',
  `gallery_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '图库Id',
  PRIMARY KEY (`id`),
  KEY `gallery_with_tags_tag_id_index` (`tag_id`),
  KEY `gallery_with_tags_gallery_id_index` (`gallery_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cnpscy_gallery_with_tags
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_logs
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_logs`;
CREATE TABLE `cnpscy_logs` (
  `log_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '日志记录表',
  `api_url` varchar(200) NOT NULL DEFAULT '' COMMENT '请求URL',
  `controller_method` varchar(200) NOT NULL DEFAULT '' COMMENT '控制器及方法',
  `route` varchar(200) NOT NULL DEFAULT '' COMMENT '路由',
  `method` varchar(200) NOT NULL DEFAULT '' COMMENT '请求方式',
  `request_data` json DEFAULT NULL COMMENT '请求参数',
  `request_headers` json DEFAULT NULL COMMENT '请求Header参数',
  `error` json DEFAULT NULL COMMENT '异常错误信息',
  `http_status` int(10) unsigned NOT NULL DEFAULT '200' COMMENT 'http status',
  `created_ip` varchar(20) NOT NULL DEFAULT '' COMMENT '创建时的IP',
  `browser_type` varchar(200) NOT NULL DEFAULT '' COMMENT '创建时浏览器类型',
  `is_delete` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除：1：删除；0：正常',
  `created_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`log_id`),
  KEY `logs_is_delete_index` (`is_delete`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cnpscy_logs
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_logs_2022_06
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_logs_2022_06`;
CREATE TABLE `cnpscy_logs_2022_06` (
  `log_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '日志记录表',
  `api_url` varchar(200) NOT NULL DEFAULT '' COMMENT '请求URL',
  `controller_method` varchar(200) NOT NULL DEFAULT '' COMMENT '控制器及方法',
  `route` varchar(200) NOT NULL DEFAULT '' COMMENT '路由',
  `method` varchar(200) NOT NULL DEFAULT '' COMMENT '请求方式',
  `request_data` json DEFAULT NULL COMMENT '请求参数',
  `request_headers` json DEFAULT NULL COMMENT '请求Header参数',
  `error` json DEFAULT NULL COMMENT '异常错误信息',
  `http_status` int(10) unsigned NOT NULL DEFAULT '200' COMMENT 'http status',
  `created_ip` varchar(20) NOT NULL DEFAULT '' COMMENT '创建时的IP',
  `browser_type` varchar(200) NOT NULL DEFAULT '' COMMENT '创建时浏览器类型',
  `is_delete` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除：1：删除；0：正常',
  `created_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`log_id`),
  KEY `logs_is_delete_index` (`is_delete`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cnpscy_logs_2022_06
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_logs_2022_07
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_logs_2022_07`;
CREATE TABLE `cnpscy_logs_2022_07` (
  `log_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '日志记录表',
  `api_url` varchar(200) NOT NULL DEFAULT '' COMMENT '请求URL',
  `controller_method` varchar(200) NOT NULL DEFAULT '' COMMENT '控制器及方法',
  `route` varchar(200) NOT NULL DEFAULT '' COMMENT '路由',
  `method` varchar(200) NOT NULL DEFAULT '' COMMENT '请求方式',
  `request_data` json DEFAULT NULL COMMENT '请求参数',
  `request_headers` json DEFAULT NULL COMMENT '请求Header参数',
  `error` json DEFAULT NULL COMMENT '异常错误信息',
  `http_status` int(10) unsigned NOT NULL DEFAULT '200' COMMENT 'http status',
  `created_ip` varchar(20) NOT NULL DEFAULT '' COMMENT '创建时的IP',
  `browser_type` varchar(200) NOT NULL DEFAULT '' COMMENT '创建时浏览器类型',
  `is_delete` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除：1：删除；0：正常',
  `created_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`log_id`),
  KEY `logs_is_delete_index` (`is_delete`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cnpscy_logs_2022_07
-- ----------------------------
BEGIN;
INSERT INTO `cnpscy_logs_2022_07` (`log_id`, `api_url`, `controller_method`, `route`, `method`, `request_data`, `request_headers`, `error`, `http_status`, `created_ip`, `browser_type`, `is_delete`, `created_time`, `updated_time`) VALUES (1, 'http://127.0.0.1:9501/admin/get_month_lists', 'getMonthList', '/admin/get_month_lists', 'GET', '{\"sign\": \"2A71D4F6CD5F52A602A5981E41876D83\", \"nonce\": \"123456789\", \"app_key\": \"34D2DEDC049E4D726D83E4E2B1F34D9171\", \"app_type\": \"0\", \"timestamp\": \"1617288677\"}', '{\"host\": [\"127.0.0.1:9501\"], \"accept\": [\"*/*\"], \"api-token\": [\"\"], \"connection\": [\"keep-alive\"], \"user-agent\": [\"ApiPOST Runtime +https://www.apipost.cn\"], \"authorization\": [\"BAiV7zMD7pLRtyTHvqjFc8+kyYDfSglDhTdun7YkvraAHXtaqWrBlq6Uwif7u5e/kS3qNNP3AgCJFqKARU37VbDsKa7ZspPUkvZbmGZGIox96aopYYXWbg4fUXAa52YWaqiU2GYij+mymgkPzX/GTSX8uICJdWX0Z+VENjRQOMg=\"], \"accept-encoding\": [\"gzip, deflate, br\"]}', '{\"msg\": \"Token已过期！\", \"code\": 401, \"file\": \"/var/www/laravel/cnpscy/hyperf-vue-admin/app/Middleware/Admin/TokenAuthMiddleware.php\", \"line\": 58}', 401, '172.20.0.1', 'ApiPOST Runtime +https://www.apipost.cn', 0, 1657105356, 1657105356);
INSERT INTO `cnpscy_logs_2022_07` (`log_id`, `api_url`, `controller_method`, `route`, `method`, `request_data`, `request_headers`, `error`, `http_status`, `created_ip`, `browser_type`, `is_delete`, `created_time`, `updated_time`) VALUES (2, 'http://127.0.0.1:9501/admin/get_month_lists', 'getMonthList', '/admin/get_month_lists', 'GET', '{\"sign\": \"2A71D4F6CD5F52A602A5981E41876D83\", \"nonce\": \"123456789\", \"app_key\": \"34D2DEDC049E4D726D83E4E2B1F34D9171\", \"app_type\": \"0\", \"timestamp\": \"1617288677\"}', '{\"host\": [\"127.0.0.1:9501\"], \"accept\": [\"*/*\"], \"api-token\": [\"\"], \"connection\": [\"keep-alive\"], \"user-agent\": [\"ApiPOST Runtime +https://www.apipost.cn\"], \"authorization\": [\"BAiV7zMD7pLRtyTHvqjFc8+kyYDfSglDhTdun7YkvraAHXtaqWrBlq6Uwif7u5e/kS3qNNP3AgCJFqKARU37VbDsKa7ZspPUkvZbmGZGIox96aopYYXWbg4fUXAa52YWaqiU2GYij+mymgkPzX/GTSX8uICJdWX0Z+VENjRQOMg=\"], \"accept-encoding\": [\"gzip, deflate, br\"]}', '{\"msg\": \"Token已过期！\", \"code\": 401, \"file\": \"/var/www/laravel/cnpscy/hyperf-vue-admin/app/Middleware/Admin/TokenAuthMiddleware.php\", \"line\": 58}', 401, '172.20.0.1', 'ApiPOST Runtime +https://www.apipost.cn', 0, 1657105364, 1657105364);
INSERT INTO `cnpscy_logs_2022_07` (`log_id`, `api_url`, `controller_method`, `route`, `method`, `request_data`, `request_headers`, `error`, `http_status`, `created_ip`, `browser_type`, `is_delete`, `created_time`, `updated_time`) VALUES (3, 'http://127.0.0.1:9501/admin/auth/me', 'me', '/admin/auth/me', 'POST', '[]', '{\"host\": [\"127.0.0.1:9501\"], \"accept\": [\"application/json, text/plain, */*\"], \"origin\": [\"http://localhost:9527\"], \"referer\": [\"http://localhost:9527/\"], \"sec-ch-ua\": [\"\\\".Not/A)Brand\\\";v=\\\"99\\\", \\\"Google Chrome\\\";v=\\\"103\\\", \\\"Chromium\\\";v=\\\"103\\\"\"], \"connection\": [\"keep-alive\"], \"user-agent\": [\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36\"], \"authorization\": [\"Bearer Fv8zOW4d8TO6bX1RMnMCoAC52Q8wQGA1gGhFL98nPg0vzpHittAXFEm94ijI7TK2kjIAyo8LeLRhmckx2NU0zTcqsxn+yC8GGxV8G+HHPoHyzumlKsYA4vel7WAodikCVZIONnbWx3oVSH0lShXGKVSOhf3MiBv9aTS5LAakjH4=\"], \"content-length\": [\"0\"], \"sec-fetch-dest\": [\"empty\"], \"sec-fetch-mode\": [\"cors\"], \"sec-fetch-site\": [\"cross-site\"], \"accept-encoding\": [\"gzip, deflate, br\"], \"accept-language\": [\"zh-CN,zh;q=0.9\"], \"sec-ch-ua-mobile\": [\"?0\"], \"sec-ch-ua-platform\": [\"\\\"macOS\\\"\"]}', '{\"msg\": \"Token已失效！\", \"code\": 401, \"file\": \"/var/www/laravel/cnpscy/hyperf-vue-admin/app/Middleware/Admin/TokenAuthMiddleware.php\", \"line\": 51}', 401, '172.20.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36', 0, 1657105466, 1657105466);
INSERT INTO `cnpscy_logs_2022_07` (`log_id`, `api_url`, `controller_method`, `route`, `method`, `request_data`, `request_headers`, `error`, `http_status`, `created_ip`, `browser_type`, `is_delete`, `created_time`, `updated_time`) VALUES (4, 'http://127.0.0.1:9501/admin/versionLogs', 'versionLogs', '/admin/versionLogs', 'GET', '[]', '{\"host\": [\"127.0.0.1:9501\"], \"accept\": [\"application/json, text/plain, */*\"], \"origin\": [\"http://localhost:9527\"], \"referer\": [\"http://localhost:9527/\"], \"sec-ch-ua\": [\"\\\".Not/A)Brand\\\";v=\\\"99\\\", \\\"Google Chrome\\\";v=\\\"103\\\", \\\"Chromium\\\";v=\\\"103\\\"\"], \"connection\": [\"keep-alive\"], \"user-agent\": [\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36\"], \"authorization\": [\"AutB3kr8f48fzVpu4qfN3FMG6DHm7yA3Dvkele+9u46rPwAckaNMsIJhrBMEMxz/xr/R3VWQxbG8VhUZBEGM/6vl001ALRchtg9S9XzbonBVB3RwxI18vpC2p2Aku6Bq0PJHqpIPxP1mPGW7A+fjw/T1WQhl6Pok5xGycsO99ZY=\"], \"sec-fetch-dest\": [\"empty\"], \"sec-fetch-mode\": [\"cors\"], \"sec-fetch-site\": [\"cross-site\"], \"accept-encoding\": [\"gzip, deflate, br\"], \"accept-language\": [\"zh-CN,zh;q=0.9\"], \"sec-ch-ua-mobile\": [\"?0\"], \"sec-ch-ua-platform\": [\"\\\"macOS\\\"\"]}', '{\"msg\": \"SQLSTATE[42S02]: Base table or view not found: 1146 Table \'hyperf-vue-admin.cnpscy_versions\' doesn\'t exist (SQL: select `version_name`, `version_number`, `version_content`, `publish_date` from `cnpscy_versions` where `cnpscy_versions`.`is_delete` = 0 order by `version_sort` desc, `publish_date` desc, `version_id` asc)\", \"code\": \"42S02\", \"file\": \"/var/www/laravel/cnpscy/hyperf-vue-admin/vendor/hyperf/database/src/Connection.php\", \"line\": 1082}', 400, '172.20.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36', 0, 1657105514, 1657105514);
INSERT INTO `cnpscy_logs_2022_07` (`log_id`, `api_url`, `controller_method`, `route`, `method`, `request_data`, `request_headers`, `error`, `http_status`, `created_ip`, `browser_type`, `is_delete`, `created_time`, `updated_time`) VALUES (5, 'http://127.0.0.1:9501/admin/admins', 'index', '/admin/admins', 'GET', '{\"page\": \"1\", \"limit\": \"10\", \"search\": \"\", \"is_check\": \"\", \"is_download\": \"0\"}', '{\"host\": [\"127.0.0.1:9501\"], \"accept\": [\"application/json, text/plain, */*\"], \"origin\": [\"http://localhost:9527\"], \"referer\": [\"http://localhost:9527/\"], \"sec-ch-ua\": [\"\\\".Not/A)Brand\\\";v=\\\"99\\\", \\\"Google Chrome\\\";v=\\\"103\\\", \\\"Chromium\\\";v=\\\"103\\\"\"], \"connection\": [\"keep-alive\"], \"user-agent\": [\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36\"], \"authorization\": [\"AutB3kr8f48fzVpu4qfN3FMG6DHm7yA3Dvkele+9u46rPwAckaNMsIJhrBMEMxz/xr/R3VWQxbG8VhUZBEGM/6vl001ALRchtg9S9XzbonBVB3RwxI18vpC2p2Aku6Bq0PJHqpIPxP1mPGW7A+fjw/T1WQhl6Pok5xGycsO99ZY=\"], \"sec-fetch-dest\": [\"empty\"], \"sec-fetch-mode\": [\"cors\"], \"sec-fetch-site\": [\"cross-site\"], \"accept-encoding\": [\"gzip, deflate, br\"], \"accept-language\": [\"zh-CN,zh;q=0.9\"], \"sec-ch-ua-mobile\": [\"?0\"], \"sec-ch-ua-platform\": [\"\\\"macOS\\\"\"]}', '{\"msg\": \"Call to undefined relationship [adminInfo] on model [App\\\\Model\\\\Rabc\\\\Admin].\", \"code\": 0, \"file\": \"/var/www/laravel/cnpscy/hyperf-vue-admin/vendor/hyperf/database/src/Model/RelationNotFoundException.php\", \"line\": 43}', 400, '172.20.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36', 0, 1657105739, 1657105739);
INSERT INTO `cnpscy_logs_2022_07` (`log_id`, `api_url`, `controller_method`, `route`, `method`, `request_data`, `request_headers`, `error`, `http_status`, `created_ip`, `browser_type`, `is_delete`, `created_time`, `updated_time`) VALUES (6, 'http://127.0.0.1:9501/admin/versions', '', '', 'GET', '{\"page\": \"1\", \"limit\": \"10\", \"search\": \"\", \"is_check\": \"\", \"is_download\": \"0\"}', '{\"host\": [\"127.0.0.1:9501\"], \"accept\": [\"application/json, text/plain, */*\"], \"origin\": [\"http://localhost:9527\"], \"referer\": [\"http://localhost:9527/\"], \"sec-ch-ua\": [\"\\\".Not/A)Brand\\\";v=\\\"99\\\", \\\"Google Chrome\\\";v=\\\"103\\\", \\\"Chromium\\\";v=\\\"103\\\"\"], \"connection\": [\"keep-alive\"], \"user-agent\": [\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36\"], \"authorization\": [\"AutB3kr8f48fzVpu4qfN3FMG6DHm7yA3Dvkele+9u46rPwAckaNMsIJhrBMEMxz/xr/R3VWQxbG8VhUZBEGM/6vl001ALRchtg9S9XzbonBVB3RwxI18vpC2p2Aku6Bq0PJHqpIPxP1mPGW7A+fjw/T1WQhl6Pok5xGycsO99ZY=\"], \"sec-fetch-dest\": [\"empty\"], \"sec-fetch-mode\": [\"cors\"], \"sec-fetch-site\": [\"cross-site\"], \"accept-encoding\": [\"gzip, deflate, br\"], \"accept-language\": [\"zh-CN,zh;q=0.9\"], \"sec-ch-ua-mobile\": [\"?0\"], \"sec-ch-ua-platform\": [\"\\\"macOS\\\"\"]}', '{\"msg\": \"Not Found\", \"code\": 0, \"file\": \"/var/www/laravel/cnpscy/hyperf-vue-admin/vendor/hyperf/http-server/src/CoreMiddleware.php\", \"line\": 173}', 400, '172.20.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36', 0, 1657105873, 1657105873);
INSERT INTO `cnpscy_logs_2022_07` (`log_id`, `api_url`, `controller_method`, `route`, `method`, `request_data`, `request_headers`, `error`, `http_status`, `created_ip`, `browser_type`, `is_delete`, `created_time`, `updated_time`) VALUES (7, 'http://127.0.0.1:9501/admin/database/tables', '', '', 'GET', '{\"search\": \"\"}', '{\"host\": [\"127.0.0.1:9501\"], \"accept\": [\"application/json, text/plain, */*\"], \"origin\": [\"http://localhost:9527\"], \"referer\": [\"http://localhost:9527/\"], \"sec-ch-ua\": [\"\\\".Not/A)Brand\\\";v=\\\"99\\\", \\\"Google Chrome\\\";v=\\\"103\\\", \\\"Chromium\\\";v=\\\"103\\\"\"], \"connection\": [\"keep-alive\"], \"user-agent\": [\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36\"], \"authorization\": [\"AutB3kr8f48fzVpu4qfN3FMG6DHm7yA3Dvkele+9u46rPwAckaNMsIJhrBMEMxz/xr/R3VWQxbG8VhUZBEGM/6vl001ALRchtg9S9XzbonBVB3RwxI18vpC2p2Aku6Bq0PJHqpIPxP1mPGW7A+fjw/T1WQhl6Pok5xGycsO99ZY=\"], \"sec-fetch-dest\": [\"empty\"], \"sec-fetch-mode\": [\"cors\"], \"sec-fetch-site\": [\"cross-site\"], \"accept-encoding\": [\"gzip, deflate, br\"], \"accept-language\": [\"zh-CN,zh;q=0.9\"], \"sec-ch-ua-mobile\": [\"?0\"], \"sec-ch-ua-platform\": [\"\\\"macOS\\\"\"]}', '{\"msg\": \"Not Found\", \"code\": 0, \"file\": \"/var/www/laravel/cnpscy/hyperf-vue-admin/vendor/hyperf/http-server/src/CoreMiddleware.php\", \"line\": 173}', 400, '172.20.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36', 0, 1657105883, 1657105883);
INSERT INTO `cnpscy_logs_2022_07` (`log_id`, `api_url`, `controller_method`, `route`, `method`, `request_data`, `request_headers`, `error`, `http_status`, `created_ip`, `browser_type`, `is_delete`, `created_time`, `updated_time`) VALUES (8, 'http://127.0.0.1:9501/%7B%7BServer%7D%7D/database/tables', '', '', 'GET', '{\"sign\": \"2A71D4F6CD5F52A602A5981E41876D83\", \"nonce\": \"123456789\", \"search\": \"cnpscy_admin_infos\", \"app_key\": \"34D2DEDC049E4D726D83E4E2B1F34D9171\", \"app_type\": \"0\", \"timestamp\": \"1617288677\"}', '{\"host\": [\"127.0.0.1:9501\"], \"accept\": [\"*/*\"], \"api-token\": [\"\"], \"connection\": [\"keep-alive\"], \"user-agent\": [\"ApiPOST Runtime +https://www.apipost.cn\"], \"authorization\": [\"BAiV7zMD7pLRtyTHvqjFc8+kyYDfSglDhTdun7YkvraAHXtaqWrBlq6Uwif7u5e/kS3qNNP3AgCJFqKARU37VbDsKa7ZspPUkvZbmGZGIox96aopYYXWbg4fUXAa52YWaqiU2GYij+mymgkPzX/GTSX8uICJdWX0Z+VENjRQOMg=\"], \"accept-encoding\": [\"gzip, deflate, br\"]}', '{\"msg\": \"Not Found\", \"code\": 0, \"file\": \"/var/www/laravel/cnpscy/hyperf-vue-admin/vendor/hyperf/http-server/src/CoreMiddleware.php\", \"line\": 173}', 400, '172.20.0.1', 'ApiPOST Runtime +https://www.apipost.cn', 0, 1657275820, 1657275820);
INSERT INTO `cnpscy_logs_2022_07` (`log_id`, `api_url`, `controller_method`, `route`, `method`, `request_data`, `request_headers`, `error`, `http_status`, `created_ip`, `browser_type`, `is_delete`, `created_time`, `updated_time`) VALUES (9, 'http://127.0.0.1:9501/admin/database/tables', 'index', '/admin/database/tables', 'GET', '{\"sign\": \"2A71D4F6CD5F52A602A5981E41876D83\", \"nonce\": \"123456789\", \"search\": \"cnpscy_admin_infos\", \"app_key\": \"34D2DEDC049E4D726D83E4E2B1F34D9171\", \"app_type\": \"0\", \"timestamp\": \"1617288677\"}', '{\"host\": [\"127.0.0.1:9501\"], \"accept\": [\"*/*\"], \"api-token\": [\"\"], \"connection\": [\"keep-alive\"], \"user-agent\": [\"ApiPOST Runtime +https://www.apipost.cn\"], \"authorization\": [\"BAiV7zMD7pLRtyTHvqjFc8+kyYDfSglDhTdun7YkvraAHXtaqWrBlq6Uwif7u5e/kS3qNNP3AgCJFqKARU37VbDsKa7ZspPUkvZbmGZGIox96aopYYXWbg4fUXAa52YWaqiU2GYij+mymgkPzX/GTSX8uICJdWX0Z+VENjRQOMg=\"], \"accept-encoding\": [\"gzip, deflate, br\"]}', '{\"msg\": \"Class DatabaseController not exist\", \"code\": 0, \"file\": \"/var/www/laravel/cnpscy/hyperf-vue-admin/vendor/hyperf/di/src/ReflectionManager.php\", \"line\": 45}', 400, '172.20.0.1', 'ApiPOST Runtime +https://www.apipost.cn', 0, 1657275838, 1657275838);
INSERT INTO `cnpscy_logs_2022_07` (`log_id`, `api_url`, `controller_method`, `route`, `method`, `request_data`, `request_headers`, `error`, `http_status`, `created_ip`, `browser_type`, `is_delete`, `created_time`, `updated_time`) VALUES (10, 'http://127.0.0.1:9501/admin/database/tables', 'index', '/admin/database/tables', 'GET', '{\"sign\": \"2A71D4F6CD5F52A602A5981E41876D83\", \"nonce\": \"123456789\", \"search\": \"cnpscy_admin_infos\", \"app_key\": \"34D2DEDC049E4D726D83E4E2B1F34D9171\", \"app_type\": \"0\", \"timestamp\": \"1617288677\"}', '{\"host\": [\"127.0.0.1:9501\"], \"accept\": [\"*/*\"], \"api-token\": [\"\"], \"connection\": [\"keep-alive\"], \"user-agent\": [\"ApiPOST Runtime +https://www.apipost.cn\"], \"authorization\": [\"BAiV7zMD7pLRtyTHvqjFc8+kyYDfSglDhTdun7YkvraAHXtaqWrBlq6Uwif7u5e/kS3qNNP3AgCJFqKARU37VbDsKa7ZspPUkvZbmGZGIox96aopYYXWbg4fUXAa52YWaqiU2GYij+mymgkPzX/GTSX8uICJdWX0Z+VENjRQOMg=\"], \"accept-encoding\": [\"gzip, deflate, br\"]}', '{\"msg\": \"Class DatabaseController not exist\", \"code\": 0, \"file\": \"/var/www/laravel/cnpscy/hyperf-vue-admin/vendor/hyperf/di/src/ReflectionManager.php\", \"line\": 45}', 400, '172.20.0.1', 'ApiPOST Runtime +https://www.apipost.cn', 0, 1657275860, 1657275860);
INSERT INTO `cnpscy_logs_2022_07` (`log_id`, `api_url`, `controller_method`, `route`, `method`, `request_data`, `request_headers`, `error`, `http_status`, `created_ip`, `browser_type`, `is_delete`, `created_time`, `updated_time`) VALUES (11, 'http://127.0.0.1:9501/admin/database/tables', 'index', '/admin/database/tables', 'GET', '{\"sign\": \"2A71D4F6CD5F52A602A5981E41876D83\", \"nonce\": \"123456789\", \"search\": \"cnpscy_admin_infos\", \"app_key\": \"34D2DEDC049E4D726D83E4E2B1F34D9171\", \"app_type\": \"0\", \"timestamp\": \"1617288677\"}', '{\"host\": [\"127.0.0.1:9501\"], \"accept\": [\"*/*\"], \"api-token\": [\"\"], \"connection\": [\"keep-alive\"], \"user-agent\": [\"ApiPOST Runtime +https://www.apipost.cn\"], \"authorization\": [\"BAiV7zMD7pLRtyTHvqjFc8+kyYDfSglDhTdun7YkvraAHXtaqWrBlq6Uwif7u5e/kS3qNNP3AgCJFqKARU37VbDsKa7ZspPUkvZbmGZGIox96aopYYXWbg4fUXAa52YWaqiU2GYij+mymgkPzX/GTSX8uICJdWX0Z+VENjRQOMg=\"], \"accept-encoding\": [\"gzip, deflate, br\"]}', '{\"msg\": \"Token已过期！\", \"code\": 401, \"file\": \"/var/www/laravel/cnpscy/hyperf-vue-admin/app/Middleware/Admin/TokenAuthMiddleware.php\", \"line\": 59}', 401, '172.20.0.1', 'ApiPOST Runtime +https://www.apipost.cn', 0, 1657275944, 1657275944);
INSERT INTO `cnpscy_logs_2022_07` (`log_id`, `api_url`, `controller_method`, `route`, `method`, `request_data`, `request_headers`, `error`, `http_status`, `created_ip`, `browser_type`, `is_delete`, `created_time`, `updated_time`) VALUES (12, 'http://127.0.0.1:9501/admin/database/tables', 'index', '/admin/database/tables', 'GET', '{\"sign\": \"2A71D4F6CD5F52A602A5981E41876D83\", \"nonce\": \"123456789\", \"search\": \"cnpscy_admin_infos\", \"app_key\": \"34D2DEDC049E4D726D83E4E2B1F34D9171\", \"app_type\": \"0\", \"timestamp\": \"1617288677\"}', '{\"host\": [\"127.0.0.1:9501\"], \"accept\": [\"*/*\"], \"api-token\": [\"\"], \"connection\": [\"keep-alive\"], \"user-agent\": [\"ApiPOST Runtime +https://www.apipost.cn\"], \"authorization\": [\"l0JCUrUa4eFpdLl0q19xDk1ot36sfsP2NWpZkmiKgxTLfJADjSFdKvgBAGfwA2FrIELQX3j8lJbeknaht1Yv+7itGg5LGCIGzVajoOf8HyBy22q94xs+w9NGoKqSfFHvzP8GTZkx7k8YVT1u0Usk3d6aBM3olxCHijkt14zhOYs=\"], \"accept-encoding\": [\"gzip, deflate, br\"]}', '{\"msg\": \"SQLSTATE[42S02]: Base table or view not found: 1146 Table \'hyperf-vue-admin.cnpscy_table_backups\' doesn\'t exist (SQL: select count(*) as aggregate from `cnpscy_table_backups` where `cnpscy_table_backups`.`is_delete` = 0)\", \"code\": \"42S02\", \"file\": \"/var/www/laravel/cnpscy/hyperf-vue-admin/vendor/hyperf/database/src/Connection.php\", \"line\": 1082}', 400, '172.20.0.1', 'ApiPOST Runtime +https://www.apipost.cn', 0, 1657275961, 1657275961);
INSERT INTO `cnpscy_logs_2022_07` (`log_id`, `api_url`, `controller_method`, `route`, `method`, `request_data`, `request_headers`, `error`, `http_status`, `created_ip`, `browser_type`, `is_delete`, `created_time`, `updated_time`) VALUES (13, 'http://127.0.0.1:9501/admin/database/backupsTables', 'backupsTables', '/admin/database/backupsTables', 'POST', '{\"sign\": \"2A71D4F6CD5F52A602A5981E41876D83\", \"nonce\": \"123456789\", \"app_key\": \"34D2DEDC049E4D726D83E4E2B1F34D9171\", \"app_type\": \"0\", \"timestamp\": \"1617288677\"}', '{\"host\": [\"127.0.0.1:9501\"], \"accept\": [\"*/*\"], \"api-token\": [\"\"], \"connection\": [\"keep-alive\"], \"user-agent\": [\"ApiPOST Runtime +https://www.apipost.cn\"], \"authorization\": [\"l0JCUrUa4eFpdLl0q19xDk1ot36sfsP2NWpZkmiKgxTLfJADjSFdKvgBAGfwA2FrIELQX3j8lJbeknaht1Yv+7itGg5LGCIGzVajoOf8HyBy22q94xs+w9NGoKqSfFHvzP8GTZkx7k8YVT1u0Usk3d6aBM3olxCHijkt14zhOYs=\"], \"content-length\": [\"0\"], \"accept-encoding\": [\"gzip, deflate, br\"]}', '{\"msg\": \"Method App\\\\Controller\\\\Admin\\\\DatabaseController::backupsTables() does not exist\", \"code\": 0, \"file\": \"/var/www/laravel/cnpscy/hyperf-vue-admin/vendor/hyperf/di/src/ReflectionManager.php\", \"line\": 47}', 400, '172.20.0.1', 'ApiPOST Runtime +https://www.apipost.cn', 0, 1657276001, 1657276001);
INSERT INTO `cnpscy_logs_2022_07` (`log_id`, `api_url`, `controller_method`, `route`, `method`, `request_data`, `request_headers`, `error`, `http_status`, `created_ip`, `browser_type`, `is_delete`, `created_time`, `updated_time`) VALUES (14, 'http://127.0.0.1:9501/admin/database/backupsTables', 'backupsTables', '/admin/database/backupsTables', 'POST', '{\"sign\": \"2A71D4F6CD5F52A602A5981E41876D83\", \"nonce\": \"123456789\", \"app_key\": \"34D2DEDC049E4D726D83E4E2B1F34D9171\", \"app_type\": \"0\", \"timestamp\": \"1617288677\"}', '{\"host\": [\"127.0.0.1:9501\"], \"accept\": [\"*/*\"], \"api-token\": [\"\"], \"connection\": [\"keep-alive\"], \"user-agent\": [\"ApiPOST Runtime +https://www.apipost.cn\"], \"authorization\": [\"l0JCUrUa4eFpdLl0q19xDk1ot36sfsP2NWpZkmiKgxTLfJADjSFdKvgBAGfwA2FrIELQX3j8lJbeknaht1Yv+7itGg5LGCIGzVajoOf8HyBy22q94xs+w9NGoKqSfFHvzP8GTZkx7k8YVT1u0Usk3d6aBM3olxCHijkt14zhOYs=\"], \"content-length\": [\"0\"], \"accept-encoding\": [\"gzip, deflate, br\"]}', '{\"msg\": \"Class \\\"App\\\\Handlers\\\\Db\\\" not found\", \"code\": 0, \"file\": \"/var/www/laravel/cnpscy/hyperf-vue-admin/app/Handlers/DatabaseHandler.php\", \"line\": 48}', 400, '172.20.0.1', 'ApiPOST Runtime +https://www.apipost.cn', 0, 1657276095, 1657276095);
INSERT INTO `cnpscy_logs_2022_07` (`log_id`, `api_url`, `controller_method`, `route`, `method`, `request_data`, `request_headers`, `error`, `http_status`, `created_ip`, `browser_type`, `is_delete`, `created_time`, `updated_time`) VALUES (15, 'http://127.0.0.1:9501/admin/database/deleteBackup', 'deleteBackup', '/admin/database/deleteBackup', 'DELETE', '{\"sign\": \"2A71D4F6CD5F52A602A5981E41876D83\", \"nonce\": \"123456789\", \"app_key\": \"34D2DEDC049E4D726D83E4E2B1F34D9171\", \"app_type\": \"0\", \"backup_id\": \"14\", \"timestamp\": \"1617288677\"}', '{\"host\": [\"127.0.0.1:9501\"], \"accept\": [\"*/*\"], \"api-token\": [\"\"], \"connection\": [\"keep-alive\"], \"user-agent\": [\"ApiPOST Runtime +https://www.apipost.cn\"], \"content-type\": [\"application/x-www-form-urlencoded\"], \"authorization\": [\"l0JCUrUa4eFpdLl0q19xDk1ot36sfsP2NWpZkmiKgxTLfJADjSFdKvgBAGfwA2FrIELQX3j8lJbeknaht1Yv+7itGg5LGCIGzVajoOf8HyBy22q94xs+w9NGoKqSfFHvzP8GTZkx7k8YVT1u0Usk3d6aBM3olxCHijkt14zhOYs=\"], \"content-length\": [\"12\"], \"accept-encoding\": [\"gzip, deflate, br\"]}', '{\"msg\": \"App\\\\Services\\\\Admin\\\\DatabaseService::deleteBackup(): Argument #1 ($backup_id) must be of type int, string given, called in /var/www/laravel/cnpscy/hyperf-vue-admin/runtime/container/proxy/App_Controller_Admin_DatabaseController.proxy.php on line 51\", \"code\": 0, \"file\": \"/var/www/laravel/cnpscy/hyperf-vue-admin/app/Services/Admin/DatabaseService.php\", \"line\": 135}', 400, '172.20.0.1', 'ApiPOST Runtime +https://www.apipost.cn', 0, 1657276146, 1657276146);
INSERT INTO `cnpscy_logs_2022_07` (`log_id`, `api_url`, `controller_method`, `route`, `method`, `request_data`, `request_headers`, `error`, `http_status`, `created_ip`, `browser_type`, `is_delete`, `created_time`, `updated_time`) VALUES (16, 'http://127.0.0.1:9501/admin/database/deleteBackup', 'deleteBackup', '/admin/database/deleteBackup', 'DELETE', '{\"sign\": \"2A71D4F6CD5F52A602A5981E41876D83\", \"nonce\": \"123456789\", \"app_key\": \"34D2DEDC049E4D726D83E4E2B1F34D9171\", \"app_type\": \"0\", \"backup_id\": \"14\", \"timestamp\": \"1617288677\"}', '{\"host\": [\"127.0.0.1:9501\"], \"accept\": [\"*/*\"], \"api-token\": [\"\"], \"connection\": [\"keep-alive\"], \"user-agent\": [\"ApiPOST Runtime +https://www.apipost.cn\"], \"content-type\": [\"application/x-www-form-urlencoded\"], \"authorization\": [\"l0JCUrUa4eFpdLl0q19xDk1ot36sfsP2NWpZkmiKgxTLfJADjSFdKvgBAGfwA2FrIELQX3j8lJbeknaht1Yv+7itGg5LGCIGzVajoOf8HyBy22q94xs+w9NGoKqSfFHvzP8GTZkx7k8YVT1u0Usk3d6aBM3olxCHijkt14zhOYs=\"], \"content-length\": [\"12\"], \"accept-encoding\": [\"gzip, deflate, br\"]}', '{\"msg\": \"App\\\\Services\\\\Admin\\\\DatabaseService::deleteBackup(): Argument #1 ($backup_id) must be of type int, string given, called in /var/www/laravel/cnpscy/hyperf-vue-admin/runtime/container/proxy/App_Controller_Admin_DatabaseController.proxy.php on line 51\", \"code\": 0, \"file\": \"/var/www/laravel/cnpscy/hyperf-vue-admin/app/Services/Admin/DatabaseService.php\", \"line\": 135}', 400, '172.20.0.1', 'ApiPOST Runtime +https://www.apipost.cn', 0, 1657276163, 1657276163);
INSERT INTO `cnpscy_logs_2022_07` (`log_id`, `api_url`, `controller_method`, `route`, `method`, `request_data`, `request_headers`, `error`, `http_status`, `created_ip`, `browser_type`, `is_delete`, `created_time`, `updated_time`) VALUES (17, 'http://127.0.0.1:9501/admin/database/deleteBackup', 'deleteBackup', '/admin/database/deleteBackup', 'DELETE', '{\"sign\": \"2A71D4F6CD5F52A602A5981E41876D83\", \"nonce\": \"123456789\", \"app_key\": \"34D2DEDC049E4D726D83E4E2B1F34D9171\", \"app_type\": \"0\", \"backup_id\": \"16\", \"timestamp\": \"1617288677\"}', '{\"host\": [\"127.0.0.1:9501\"], \"accept\": [\"*/*\"], \"api-token\": [\"\"], \"connection\": [\"keep-alive\"], \"user-agent\": [\"ApiPOST Runtime +https://www.apipost.cn\"], \"content-type\": [\"application/x-www-form-urlencoded\"], \"authorization\": [\"l0JCUrUa4eFpdLl0q19xDk1ot36sfsP2NWpZkmiKgxTLfJADjSFdKvgBAGfwA2FrIELQX3j8lJbeknaht1Yv+7itGg5LGCIGzVajoOf8HyBy22q94xs+w9NGoKqSfFHvzP8GTZkx7k8YVT1u0Usk3d6aBM3olxCHijkt14zhOYs=\"], \"content-length\": [\"12\"], \"accept-encoding\": [\"gzip, deflate, br\"]}', '{\"msg\": \"Undefined array key \\\"trashed\\\"\", \"code\": 0, \"file\": \"/var/www/laravel/cnpscy/hyperf-vue-admin/vendor/hyperf/database/src/Model/Concerns/HasEvents.php\", \"line\": 149}', 400, '172.20.0.1', 'ApiPOST Runtime +https://www.apipost.cn', 0, 1657276168, 1657276168);
INSERT INTO `cnpscy_logs_2022_07` (`log_id`, `api_url`, `controller_method`, `route`, `method`, `request_data`, `request_headers`, `error`, `http_status`, `created_ip`, `browser_type`, `is_delete`, `created_time`, `updated_time`) VALUES (18, 'http://127.0.0.1:9501/admin/database/deleteBackup', 'deleteBackup', '/admin/database/deleteBackup', 'DELETE', '{\"sign\": \"2A71D4F6CD5F52A602A5981E41876D83\", \"nonce\": \"123456789\", \"app_key\": \"34D2DEDC049E4D726D83E4E2B1F34D9171\", \"app_type\": \"0\", \"backup_id\": \"16\", \"timestamp\": \"1617288677\"}', '{\"host\": [\"127.0.0.1:9501\"], \"accept\": [\"*/*\"], \"api-token\": [\"\"], \"connection\": [\"keep-alive\"], \"user-agent\": [\"ApiPOST Runtime +https://www.apipost.cn\"], \"content-type\": [\"application/x-www-form-urlencoded\"], \"authorization\": [\"l0JCUrUa4eFpdLl0q19xDk1ot36sfsP2NWpZkmiKgxTLfJADjSFdKvgBAGfwA2FrIELQX3j8lJbeknaht1Yv+7itGg5LGCIGzVajoOf8HyBy22q94xs+w9NGoKqSfFHvzP8GTZkx7k8YVT1u0Usk3d6aBM3olxCHijkt14zhOYs=\"], \"content-length\": [\"12\"], \"accept-encoding\": [\"gzip, deflate, br\"]}', '{\"msg\": \"Undefined array key \\\"trashed\\\"\", \"code\": 0, \"file\": \"/var/www/laravel/cnpscy/hyperf-vue-admin/vendor/hyperf/database/src/Model/Concerns/HasEvents.php\", \"line\": 149}', 400, '172.20.0.1', 'ApiPOST Runtime +https://www.apipost.cn', 0, 1657276207, 1657276207);
INSERT INTO `cnpscy_logs_2022_07` (`log_id`, `api_url`, `controller_method`, `route`, `method`, `request_data`, `request_headers`, `error`, `http_status`, `created_ip`, `browser_type`, `is_delete`, `created_time`, `updated_time`) VALUES (19, 'http://127.0.0.1:9501/admin/database/tables', 'index', '/admin/database/tables', 'GET', '{\"sign\": \"2A71D4F6CD5F52A602A5981E41876D83\", \"nonce\": \"123456789\", \"search\": \"cnpscy_admin_infos\", \"app_key\": \"34D2DEDC049E4D726D83E4E2B1F34D9171\", \"app_type\": \"0\", \"timestamp\": \"1617288677\"}', '{\"host\": [\"127.0.0.1:9501\"], \"accept\": [\"*/*\"], \"api-token\": [\"\"], \"connection\": [\"keep-alive\"], \"user-agent\": [\"ApiPOST Runtime +https://www.apipost.cn\"], \"authorization\": [\"l0JCUrUa4eFpdLl0q19xDk1ot36sfsP2NWpZkmiKgxTLfJADjSFdKvgBAGfwA2FrIELQX3j8lJbeknaht1Yv+7itGg5LGCIGzVajoOf8HyBy22q94xs+w9NGoKqSfFHvzP8GTZkx7k8YVT1u0Usk3d6aBM3olxCHijkt14zhOYs=\"], \"accept-encoding\": [\"gzip, deflate, br\"]}', '{\"msg\": \"SQLSTATE[42S22]: Column not found: 1054 Unknown column \'cnpscy_table_backups.deleted_at\' in \'where clause\' (SQL: select count(*) as aggregate from `cnpscy_table_backups` where `cnpscy_table_backups`.`deleted_at` is null)\", \"code\": \"42S22\", \"file\": \"/var/www/laravel/cnpscy/hyperf-vue-admin/vendor/hyperf/database/src/Connection.php\", \"line\": 1082}', 400, '172.20.0.1', 'ApiPOST Runtime +https://www.apipost.cn', 0, 1657276627, 1657276627);
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_migrations
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_migrations`;
CREATE TABLE `cnpscy_migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cnpscy_migrations
-- ----------------------------
BEGIN;
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (1, '2020_08_10_182650_create_admins_table', 1);
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (2, '2020_08_10_182726_create_admin_infos_table', 1);
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (3, '2020_08_10_182732_create_admin_logs_table', 1);
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (4, '2020_08_10_182738_create_admin_login_logs_table', 1);
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (5, '2020_08_10_182755_create_admin_menus_table', 1);
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (6, '2020_08_10_182803_create_admin_roles_table', 1);
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (7, '2020_08_10_182831_create_configs_table', 1);
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (8, '2020_08_10_182837_create_banners_table', 1);
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (9, '2020_08_10_182850_create_articles_table', 1);
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (10, '2020_08_10_182859_create_article_categories_table', 1);
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (11, '2020_08_10_182929_create_article_contents_table', 1);
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (12, '2020_08_10_182939_create_friendlinks_table', 2);
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (13, '2020_08_10_182950_create_notices_table', 2);
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (14, '2020_08_10_182959_create_role_with_menus_table', 2);
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (15, '2020_08_10_183013_create_upload_files_table', 2);
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (16, '2020_08_10_183020_create_upload_groups_table', 2);
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (17, '2020_08_10_183032_create_protocols_table', 2);
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (18, '2020_08_25_151204_create_users_table', 2);
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (19, '2020_08_25_151205_create_user_infos_table', 2);
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (20, '2020_08_25_151511_create_bbs_dynamics_table', 2);
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (21, '2020_08_25_151512_create_bbs_dynamic_praises_table', 2);
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (22, '2020_08_25_151523_create_bbs_dynamic_comments_table', 2);
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (23, '2020_08_25_151536_create_bbs_dynamic_collections_table', 2);
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (24, '2020_08_25_151615_create_bbs_chat_records_table', 2);
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (25, '2020_08_25_151630_create_bbs_guestbooks_table', 2);
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (26, '2020_08_25_151650_create_bbs_friends_table', 2);
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (27, '2020_08_25_151652_create_bbs_friend_groups_table', 2);
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (28, '2020_08_25_151705_create_bbs_friend_applies_table', 2);
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (29, '2020_08_25_151719_create_bbs_groups_table', 2);
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (30, '2020_08_25_151729_create_bbs_group_users_table', 2);
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (31, '2020_08_25_151738_create_bbs_group_chats_table', 2);
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (32, '2020_08_25_151748_create_bbs_group_notices_table', 2);
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (33, '2020_08_25_160317_create_bbs_user_settings_table', 2);
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (34, '2020_08_25_161238_create_bbs_albums_table', 2);
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (35, '2020_08_25_161239_create_bbs_album_users_table', 2);
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (36, '2020_08_25_161630_create_adverts_table', 2);
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (37, '2020_09_15_152619_create_notifies', 2);
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (38, '2020_09_15_153803_create_system_notify_users', 2);
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (39, '2020_09_29_105920_create_gallery_tags', 2);
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (40, '2020_09_29_105927_create_galleries', 2);
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (41, '2020_09_29_105938_create_gallery_details', 2);
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (42, '2020_09_29_111311_create_gallery_with_tags', 2);
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (43, '2020_11_20_180501_create_bbs_user_signs', 2);
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (44, '2020_11_20_180502_create_bbs_user_grades', 2);
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (45, '2020_11_20_180503_create_bbs_user_experience_records', 2);
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (46, '2020_11_20_180912_create_user_email_verifies', 2);
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (47, '2020_11_20_181117_create_user_login_logs', 2);
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (48, '2020_11_20_181303_create_user_logs', 3);
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (49, '2020_11_20_181530_create_user_today_online_records', 3);
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (50, '2020_11_20_181717_create_user_safety_problems', 3);
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (51, '2020_11_20_181844_create_user_with_problems', 3);
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (52, '2020_12_01_143341_create_admin_with_roles', 3);
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (53, '2020_12_01_143542_create_role_with_menus', 3);
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (54, '2021_04_01_104329_create_admin_routes_table', 3);
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (55, '2021_04_01_170136_create_apps_table', 3);
INSERT INTO `cnpscy_migrations` (`id`, `migration`, `batch`) VALUES (56, '2021_12_22_112910_create_logs_table', 3);
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_notices
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_notices`;
CREATE TABLE `cnpscy_notices` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cnpscy_notices
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_notifies
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_notifies`;
CREATE TABLE `cnpscy_notifies` (
  `notify_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '系统消息通知记录表',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '会员Id',
  `is_read` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否已读：1：是；0：否',
  `notify_type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '通知类型：0.系统通知/公告；1.提醒；2.私信',
  `target_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '目标Id(比如动态ID)',
  `target_type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '目标类型：0.动态',
  `sender_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '发送者Id',
  `sender_type` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '发送者类型：0.系统通知；1.指定会员',
  `created_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `notify_content` text COMMENT '通知内容',
  PRIMARY KEY (`notify_id`),
  KEY `notifies_user_id_index` (`user_id`),
  KEY `notifies_is_read_index` (`is_read`),
  KEY `notifies_notify_type_index` (`notify_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cnpscy_notifies
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_protocols
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_protocols`;
CREATE TABLE `cnpscy_protocols` (
  `protocol_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '协议表',
  `protocol_type` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '协议类型',
  `protocol_title` varchar(200) NOT NULL DEFAULT '' COMMENT '协议标题',
  `protocol_content` text COMMENT '内容',
  `is_delete` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除：1：删除；0：正常',
  `created_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`protocol_id`),
  KEY `protocols_is_delete_index` (`is_delete`),
  KEY `protocols_protocol_type_index` (`protocol_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cnpscy_protocols
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_role_with_menus
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_role_with_menus`;
CREATE TABLE `cnpscy_role_with_menus` (
  `with_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '角色Id',
  `menu_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '菜单Id',
  PRIMARY KEY (`with_id`) USING BTREE,
  KEY `role_with_menus_role_id_index` (`role_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=217 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of cnpscy_role_with_menus
-- ----------------------------
BEGIN;
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (1, 1, 1);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (2, 1, 2);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (3, 1, 3);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (4, 1, 4);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (5, 1, 5);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (6, 1, 6);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (7, 1, 7);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (8, 1, 8);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (9, 1, 9);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (10, 1, 10);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (11, 1, 11);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (12, 1, 12);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (13, 1, 13);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (42, 1, 14);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (43, 1, 15);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (44, 1, 16);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (45, 1, 17);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (46, 1, 18);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (47, 1, 19);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (48, 1, 20);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (49, 1, 21);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (50, 1, 22);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (51, 1, 23);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (52, 1, 24);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (53, 1, 25);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (54, 1, 26);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (55, 1, 27);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (56, 1, 28);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (57, 1, 29);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (58, 1, 30);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (59, 1, 31);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (60, 1, 32);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (61, 1, 33);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (62, 1, 34);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (63, 1, 35);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (64, 1, 36);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (65, 1, 37);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (66, 1, 38);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (67, 1, 39);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (68, 1, 40);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (69, 1, 41);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (70, 1, 42);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (71, 1, 43);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (72, 1, 44);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (73, 1, 45);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (74, 1, 46);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (75, 1, 47);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (76, 1, 48);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (77, 1, 49);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (78, 1, 50);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (79, 1, 51);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (80, 1, 52);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (81, 1, 53);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (99, 2, 1);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (100, 2, 2);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (101, 2, 3);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (102, 2, 4);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (114, 1, 57);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (115, 1, 58);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (116, 1, 59);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (117, 1, 60);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (118, 1, 61);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (119, 1, 62);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (120, 1, 63);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (121, 1, 64);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (122, 1, 65);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (123, 1, 66);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (124, 1, 55);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (125, 1, 56);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (126, 12, 1);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (127, 12, 2);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (128, 12, 3);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (129, 12, 4);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (130, 12, 54);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (131, 15, 1);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (132, 15, 2);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (133, 14, 1);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (134, 15, 6);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (135, 14, 2);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (136, 15, 7);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (137, 14, 6);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (138, 15, 14);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (139, 14, 7);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (140, 15, 15);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (141, 14, 14);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (142, 15, 16);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (143, 14, 15);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (144, 15, 17);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (145, 14, 16);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (146, 15, 18);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (147, 14, 17);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (148, 15, 19);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (149, 14, 18);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (150, 15, 58);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (151, 14, 19);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (152, 14, 58);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (153, 15, 3);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (154, 15, 4);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (155, 15, 8);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (156, 15, 9);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (157, 15, 10);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (158, 15, 11);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (159, 15, 12);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (160, 15, 13);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (161, 15, 20);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (162, 15, 21);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (163, 15, 22);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (164, 15, 23);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (165, 15, 24);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (166, 15, 25);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (167, 15, 26);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (168, 15, 27);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (169, 15, 28);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (170, 15, 29);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (171, 15, 30);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (172, 15, 31);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (173, 15, 32);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (174, 15, 33);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (175, 15, 34);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (176, 15, 35);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (177, 15, 36);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (178, 15, 37);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (179, 15, 38);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (180, 15, 39);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (181, 15, 40);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (182, 15, 41);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (183, 15, 42);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (184, 15, 43);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (185, 15, 44);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (186, 15, 45);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (187, 15, 46);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (188, 15, 47);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (189, 15, 48);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (190, 15, 49);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (191, 15, 50);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (192, 15, 51);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (193, 15, 52);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (194, 15, 53);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (195, 15, 54);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (196, 15, 55);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (197, 15, 56);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (198, 15, 57);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (199, 15, 59);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (200, 15, 60);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (201, 15, 61);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (202, 15, 62);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (203, 15, 63);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (204, 15, 64);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (205, 15, 65);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (206, 15, 66);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (207, 1, 67);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (208, 1, 68);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (209, 1, 69);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (210, 1, 70);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (211, 1, 71);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (212, 1, 72);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (213, 1, 73);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (214, 1, 74);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (215, 1, 75);
INSERT INTO `cnpscy_role_with_menus` (`with_id`, `role_id`, `menu_id`) VALUES (216, 1, 76);
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_system_notify_users
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_system_notify_users`;
CREATE TABLE `cnpscy_system_notify_users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '系统消息与会员的已读记录表',
  `notify_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '通知Id',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '会员Id',
  `created_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `system_notify_users_user_id_index` (`user_id`),
  KEY `system_notify_users_notify_id_index` (`notify_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cnpscy_system_notify_users
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_table_backups
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_table_backups`;
CREATE TABLE `cnpscy_table_backups` (
  `backup_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '数据库备份记录表',
  `admin_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '操作人',
  `created_ip` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '创建IP',
  `tables_name` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '备份的表名',
  `file_size` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文件大小：字节',
  `file_num` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文件数量',
  `backup_files` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '备份的文件',
  `is_delete` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除：0.否；1.是',
  `created_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`backup_id`) USING BTREE,
  KEY `cnpscy_table_backups_is_delete_index` (`is_delete`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of cnpscy_table_backups
-- ----------------------------
BEGIN;
INSERT INTO `cnpscy_table_backups` (`backup_id`, `admin_id`, `created_ip`, `tables_name`, `file_size`, `file_num`, `backup_files`, `is_delete`, `created_time`, `updated_time`) VALUES (16, 0, '172.20.0.1', 'cnpscy_admin_infos,cnpscy_admin_login_logs,cnpscy_admin_logs,cnpscy_admin_menus,cnpscy_admin_roles,cnpscy_admin_routes,cnpscy_admin_with_roles,cnpscy_admins,cnpscy_adverts,cnpscy_apps,cnpscy_article_categories,cnpscy_article_contents,cnpscy_articles,cnpscy_banners,cnpscy_bbs_album_users,cnpscy_bbs_albums,cnpscy_bbs_chat_records,cnpscy_bbs_dynamic_collections,cnpscy_bbs_dynamic_comments,cnpscy_bbs_dynamic_praises,cnpscy_bbs_dynamics,cnpscy_bbs_friend_applies,cnpscy_bbs_friend_groups,cnpscy_bbs_friends,cnpscy_bbs_group_chats,cnpscy_bbs_group_notices,cnpscy_bbs_group_users,cnpscy_bbs_groups,cnpscy_bbs_guestbooks,cnpscy_bbs_user_experience_records,cnpscy_bbs_user_grades,cnpscy_bbs_user_settings,cnpscy_bbs_user_signs,cnpscy_configs,cnpscy_friendlinks,cnpscy_galleries,cnpscy_gallery_details,cnpscy_gallery_tags,cnpscy_gallery_with_tags,cnpscy_logs,cnpscy_migrations,cnpscy_notices,cnpscy_notifies,cnpscy_protocols,cnpscy_role_with_menus,cnpscy_system_notify_users,cnpscy_table_backups,cnpscy_upload_files,cnpscy_upload_groups,cnpscy_user_email_verifies,cnpscy_user_infos,cnpscy_user_login_logs,cnpscy_user_logs,cnpscy_user_safety_problems,cnpscy_user_today_online_records,cnpscy_user_with_problems,cnpscy_users,cnpscy_versions', 93918, 1, '/var/www/laravel/cnpscy/hyperf-vue-admin/storage/database/backups/hyperf-vue-admin=2022-07-08=182836=1TA-1657276116=1.sql', 1, 1657276116, 1657277038);
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_upload_files
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_upload_files`;
CREATE TABLE `cnpscy_upload_files` (
  `file_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '文件表',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '会员Id',
  `storage` varchar(200) NOT NULL DEFAULT '' COMMENT '存储方式',
  `host_url` varchar(200) NOT NULL DEFAULT '' COMMENT '存储域名',
  `file_name` varchar(200) NOT NULL DEFAULT '' COMMENT '文件路径',
  `file_size` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文件大小(字节)',
  `file_type` varchar(200) NOT NULL DEFAULT '' COMMENT '文件类型',
  `file_extension` varchar(200) NOT NULL DEFAULT '' COMMENT '文件扩展名',
  `is_delete` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除：1：删除；0：正常',
  `created_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`file_id`),
  KEY `upload_files_user_id_index` (`user_id`),
  KEY `upload_files_is_delete_index` (`is_delete`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cnpscy_upload_files
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_upload_groups
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_upload_groups`;
CREATE TABLE `cnpscy_upload_groups` (
  `group_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '分类id',
  `group_type` varchar(10) NOT NULL DEFAULT '' COMMENT '文件类型',
  `group_name` varchar(30) NOT NULL DEFAULT '' COMMENT '分类名称',
  `group_sort` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '分类排序(数字越小越靠前)',
  `is_delete` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除',
  `created_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`group_id`) USING BTREE,
  KEY `type_index` (`group_type`) USING BTREE,
  KEY `is_delete` (`is_delete`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10042 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='文件库分组记录表';

-- ----------------------------
-- Records of cnpscy_upload_groups
-- ----------------------------
BEGIN;
INSERT INTO `cnpscy_upload_groups` (`group_id`, `group_type`, `group_name`, `group_sort`, `is_delete`, `created_time`, `updated_time`) VALUES (10037, '', '1432432', 0, 0, 1621654881, 1621654881);
INSERT INTO `cnpscy_upload_groups` (`group_id`, `group_type`, `group_name`, `group_sort`, `is_delete`, `created_time`, `updated_time`) VALUES (10038, '', '2021-05', 0, 0, 1621655053, 1621655053);
INSERT INTO `cnpscy_upload_groups` (`group_id`, `group_type`, `group_name`, `group_sort`, `is_delete`, `created_time`, `updated_time`) VALUES (10039, '', '132131', 0, 1, 1621655186, 1621664514);
INSERT INTO `cnpscy_upload_groups` (`group_id`, `group_type`, `group_name`, `group_sort`, `is_delete`, `created_time`, `updated_time`) VALUES (10040, '', '112312', 0, 0, 1621661791, 1621662347);
INSERT INTO `cnpscy_upload_groups` (`group_id`, `group_type`, `group_name`, `group_sort`, `is_delete`, `created_time`, `updated_time`) VALUES (10041, '', '3', 0, 1, 1621662341, 1657103917);
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_user_email_verifies
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_user_email_verifies`;
CREATE TABLE `cnpscy_user_email_verifies` (
  `verify_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '邮箱验证表',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '会员Id',
  `user_email` varchar(100) NOT NULL DEFAULT '' COMMENT '邮箱',
  `verify_token` varchar(256) NOT NULL DEFAULT '' COMMENT '验证TOKEN',
  `auth_email` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '邮箱验证状态：0：否，1：是',
  `created_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`verify_id`),
  KEY `user_email_verifies_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cnpscy_user_email_verifies
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_user_infos
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_user_infos`;
CREATE TABLE `cnpscy_user_infos` (
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '会员Id',
  `nick_name` varchar(100) NOT NULL DEFAULT '' COMMENT '昵称',
  `user_uuid` varchar(36) NOT NULL DEFAULT '' COMMENT 'uuid',
  `personal_signature` varchar(200) NOT NULL DEFAULT '' COMMENT '个性签名',
  `user_avatar` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '头像',
  `birthday` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '出生日期',
  `user_sex` tinyint(1) unsigned NOT NULL DEFAULT '2' COMMENT '性别：0：男；1：女；2.保密',
  `is_public` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否公开：0：私密；1：公开；2.密码访问',
  `visit_password` varchar(60) NOT NULL DEFAULT '' COMMENT '访问密码',
  `created_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  KEY `user_infos_user_id_index` (`user_id`),
  KEY `user_infos_is_public_index` (`is_public`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cnpscy_user_infos
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_user_login_logs
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_user_login_logs`;
CREATE TABLE `cnpscy_user_login_logs` (
  `log_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '会员登录日志记录表',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Id',
  `log_status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '登录状态：1：成功；0：失败',
  `is_public` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否展示：1.展示；0.会员删除；2.管理员删除',
  `log_description` varchar(200) NOT NULL DEFAULT '' COMMENT '描述',
  `created_ip` varchar(20) NOT NULL DEFAULT '' COMMENT '创建时的IP',
  `browser_type` varchar(200) NOT NULL DEFAULT '' COMMENT '创建时浏览器类型',
  `is_delete` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除：1：删除；0：正常',
  `created_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`log_id`),
  KEY `user_login_logs_user_id_index` (`user_id`),
  KEY `user_login_logs_is_public_index` (`is_public`),
  KEY `user_login_logs_is_delete_index` (`is_delete`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cnpscy_user_login_logs
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_user_logs
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_user_logs`;
CREATE TABLE `cnpscy_user_logs` (
  `log_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '会员登录日志记录表',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Id',
  `log_type` smallint(6) NOT NULL DEFAULT '0' COMMENT '日志类型【0.登陆；1.退出；2.签到；……】',
  `login_type` smallint(6) NOT NULL DEFAULT '0' COMMENT '登录类型【0.普通登录】',
  `log_status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态：1：成功；0：失败',
  `log_description` varchar(200) NOT NULL DEFAULT '' COMMENT '描述',
  `created_ip` varchar(20) NOT NULL DEFAULT '' COMMENT '创建时的IP',
  `browser_type` varchar(200) NOT NULL DEFAULT '' COMMENT '创建时浏览器类型',
  `is_delete` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除：1：删除；0：正常',
  `created_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `request_data` json DEFAULT NULL COMMENT '请求参数',
  `extend_json` json DEFAULT NULL COMMENT '扩展信息',
  PRIMARY KEY (`log_id`),
  KEY `user_logs_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cnpscy_user_logs
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_user_safety_problems
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_user_safety_problems`;
CREATE TABLE `cnpscy_user_safety_problems` (
  `problem_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '安全问题表',
  `problem_name` varchar(200) NOT NULL DEFAULT '' COMMENT '安全问题名称',
  `is_delete` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除：1：删除；0：正常',
  `created_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`problem_id`),
  KEY `user_safety_problems_is_delete_index` (`is_delete`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cnpscy_user_safety_problems
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_user_today_online_records
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_user_today_online_records`;
CREATE TABLE `cnpscy_user_today_online_records` (
  `record_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '每天在线会员的记录表',
  `day_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '当天时间戳 - 年月日即可',
  `user_json` json DEFAULT NULL COMMENT '会员Id记录JSON格式',
  PRIMARY KEY (`record_id`),
  KEY `user_today_online_records_day_time_index` (`day_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cnpscy_user_today_online_records
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_user_with_problems
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_user_with_problems`;
CREATE TABLE `cnpscy_user_with_problems` (
  `with_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '会员关联安全问题表',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '会员主键',
  `problem_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '安全问题主键',
  `question_answers` varchar(256) NOT NULL DEFAULT '' COMMENT '安全问题答案',
  `created_ip` varchar(20) NOT NULL DEFAULT '' COMMENT '创建时的IP',
  `created_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`with_id`),
  KEY `user_with_problems_user_id_index` (`user_id`),
  KEY `user_with_problems_problem_id_index` (`problem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cnpscy_user_with_problems
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_users
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_users`;
CREATE TABLE `cnpscy_users` (
  `user_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '会员表',
  `user_name` varchar(100) NOT NULL DEFAULT '' COMMENT '账户',
  `user_email` varchar(100) NOT NULL DEFAULT '' COMMENT '邮箱',
  `password` varchar(60) NOT NULL DEFAULT '' COMMENT '登录密码',
  `is_check` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态：1：启用；2：禁用',
  `is_delete` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除：1：删除；0：正常',
  PRIMARY KEY (`user_id`),
  KEY `users_is_check_index` (`is_check`),
  KEY `users_is_delete_index` (`is_delete`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cnpscy_users
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cnpscy_versions
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_versions`;
CREATE TABLE `cnpscy_versions` (
  `version_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '版本表',
  `version_name` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '名称',
  `version_number` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '版本号',
  `version_content` text COLLATE utf8mb4_unicode_ci COMMENT '内容',
  `created_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `is_delete` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除',
  `version_sort` smallint(3) unsigned DEFAULT '0' COMMENT '排序',
  `publish_date` datetime DEFAULT NULL COMMENT '版本的发布时间',
  PRIMARY KEY (`version_id`) USING BTREE,
  KEY `cnpscy_versions_is_delete_index` (`is_delete`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of cnpscy_versions
-- ----------------------------
BEGIN;
INSERT INTO `cnpscy_versions` (`version_id`, `version_name`, `version_number`, `version_content`, `created_time`, `updated_time`, `is_delete`, `version_sort`, `publish_date`) VALUES (1, '基础版本发布', '1.0.0', '[1.0.0 分支](https://gitee.com/clown-passerby-community/laravel-vue-admin/tree/1.0.0)', 1610002630, 1610010586, 0, 1, '2021-01-05 16:56:32');
INSERT INTO `cnpscy_versions` (`version_id`, `version_name`, `version_number`, `version_content`, `created_time`, `updated_time`, `is_delete`, `version_sort`, `publish_date`) VALUES (2, '1.0.1', '1.0.1', '[1.0.1 分支](https://gitee.com/clown-passerby-community/laravel-vue-admin/tree/1.0.1)\n\n* **文章详情完善**；\n* **文章列表：状态变更设置；**\n* **默认图片组件的border-radius可随意传参设置;**\n* **系统设置下的配置管理、友情链接、Banner文件目录更换至根目录。**', 1610002657, 1610010613, 0, 2, '2021-01-06 00:03:18');
INSERT INTO `cnpscy_versions` (`version_id`, `version_name`, `version_number`, `version_content`, `created_time`, `updated_time`, `is_delete`, `version_sort`, `publish_date`) VALUES (3, '1.0.2', '1.0.2', '[1.0.2 分支](https://gitee.com/clown-passerby-community/laravel-vue-admin/tree/1.0.2)\n\n* **表单内的下拉框，默认100%宽度占比；**\n* **表单详情排版label不同页面不同宽度调整；**\n* **列表页状态对应图标展示效果；**\n* **scope.row替换为row简化代码写法；**\n* **菜单管理列表页：隐藏与展示图标对应效果。**', 1610002669, 1610010640, 0, 3, '2021-01-06 13:03:24');
INSERT INTO `cnpscy_versions` (`version_id`, `version_name`, `version_number`, `version_content`, `created_time`, `updated_time`, `is_delete`, `version_sort`, `publish_date`) VALUES (4, '1.0.3', '1.0.3', '[1.0.3 分支](https://gitee.com/clown-passerby-community/laravel-vue-admin/tree/1.0.3)\n\n* **版本管理；**\n* **form input 宽度100%设定；**\n* **引入 moment；**\n* **vue-element-admin Markdown 输出样式排版问题修复方案；**\n* **首页：版本历史记录渲染；**\n* **markdown 渲染样式。**', 1610095906, 1610096043, 0, 4, '2021-01-07 18:50:02');
INSERT INTO `cnpscy_versions` (`version_id`, `version_name`, `version_number`, `version_content`, `created_time`, `updated_time`, `is_delete`, `version_sort`, `publish_date`) VALUES (5, '1.0.4', '1.0.4', '[1.0.4 分支](https://gitee.com/clown-passerby-community/laravel-vue-admin/tree/1.0.4)\n\n* **同步使用多语言包;**\n* **update readme。**', 1610095993, 1610096019, 0, 5, '2021-01-07 23:00:10');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
