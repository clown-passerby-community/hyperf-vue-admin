<?php

declare(strict_types=1);

use Hyperf\Crontab\Crontab;

return [
    // 是否开启定时任务
    'enable' => true,
    // 通过配置文件定义的定时任务
    'crontab' => [
        // Callback类型定时任务（默认）
        // $crontab->setName('AutoTableBuild')->setRule('30 1 1 * *')->setCallback([App\Crontab\AutoTableBuild::class, 'execute'])->setMemo('这是一个示例的定时任务'),
    ],
];
