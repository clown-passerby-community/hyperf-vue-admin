<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class CreateLogsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (Schema::hasTable('logs')) return;
        // 【按月分表】
        // 作为基础表，所有的分表生成按照词表的结构进行复制并重命名
        Schema::create('logs', function (Blueprint $table) {
            // 指定表存储引擎
            $table->engine = 'InnoDB';
            $table->bigIncrements('log_id')->comment('日志记录表');
            $table->string('api_url', 200)->default('')->comment('请求URL');
            $table->string('controller_method', 200)->default('')->comment('控制器及方法');
            $table->string('route', 200)->default('')->comment('路由');
            $table->string('method', 200)->default('')->comment('请求方式');
            $table->json('request_data')->nullable()->comment('请求参数');
            $table->json('request_headers')->nullable()->comment('请求Header参数');
            $table->json('error')->nullable()->comment('异常错误信息');
            $table->integer('http_status')->unsigned()->default(200)->comment('http status');
            $table->string('created_ip', 20)->default('')->comment('创建时的IP');
            $table->string('browser_type', 200)->default('')->comment('创建时浏览器类型');
            $table->boolean('is_delete')->unsigned()->default(0)->comment('是否删除：1：删除；0：正常');
            $table->integer('created_time')->unsigned()->default(0)->comment('创建时间');
            $table->integer('updated_time')->unsigned()->default(0)->comment('更新时间');
            // 索引
            $table->index('is_delete');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('logs');
    }
}
